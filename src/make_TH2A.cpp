/**
  * @file make_TH2A.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2018-09-24
  * @brief This is the generic version of make_TH2A_simple.
  *        The script allows you to define "tasks" in a config file.
  *        Each task loads a number of histograms (potentially from different files) and creates a binning from them.
  *        For this, it uses the logic of defining a bin from the minimum sum of weights in any of the histograms (see get_least_entries_in_range).
  */
//ROOT
#include "TStopwatch.h"
//boost
#include "boost/range/iterator_range.hpp"
//local
#include <TH2A.h>
#include <IOjuggler.h>

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //get some stuff, IOjuggler takes care of this
  //parse options pass to executable
  auto options = IOjuggler::parse_options(argc, argv, "c:d:ho:v:");
  MessageService msgsvc("make_TH2A",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  TH2A::SetVerbosity(options.get<int>("verbosity"));
  //get ptree
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  auto wd = options.get<std::string>("workdir");
  TFile* outfile = TFile::Open((wd+"/"+options.get<std::string>("outfilename")).data(),"RECREATE");

  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  //main part starts here:
  //loop tasks, get histograms and make adaptive binning from them. Save the TH2As in the outputfile
  for(const auto& task : configtree.get_child("tasks")){
    TStopwatch task_clock;
    task_clock.Start();
    std::vector<TH2D> input_histograms;
    msgsvc.infomsg("Input for this task: ");
    for(const auto& hist : task.second.get_child("histograms")){
      const auto filename = hist.second.get<std::string>("filename");
      const auto histname = hist.second.get("histname",hist.first);
      msgsvc.infomsg("File name      : " + filename);
      msgsvc.infomsg("Histogram name : " + histname);
      auto infile = IOjuggler::get_file(filename,wd);
      input_histograms.push_back(*IOjuggler::get_obj<TH2D>(infile,histname));
      if(auto new_hist_name = hist.second.get_optional<std::string>("NewHistName"))
        input_histograms.back().SetName((*new_hist_name).data());
      else
        input_histograms.back().SetName((histname+"_adaptive").data());
    }
    TH2A h1_adaptive;
    h1_adaptive.MakeBinning(input_histograms,task.second.get("NEventsPerBin",100.0));
    outfile->cd();
    h1_adaptive.SetName(input_histograms.front().GetName());
    h1_adaptive.Write();
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) h1_adaptive.Print();
    if(auto binning_name = task.second.get_optional<std::string>("SaveBinning"))
      h1_adaptive.WriteBinning(*binning_name);
    if(task.second.get_optional<bool>("SaveAllHists")){
      for(const auto& histo : boost::make_iterator_range(input_histograms.begin()+1,input_histograms.end())){
        auto adaptive_histo = h1_adaptive;
        adaptive_histo.FillFromHist(histo);
        adaptive_histo.Write(histo.GetName());        
      }
    }
    msgsvc.infomsg("Task "+ task.first +" finished.");
    task_clock.Stop();
    if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
      task_clock.Print();
  }
  outfile->Close();//avoid double free corruption
  msgsvc.infomsg("Done with all tasks.");
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}

