#ifndef TH2A_H
#define TH2A_H
//ROOT
#include "TNamed.h"
#include "Rtypes.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "Riostream.h"
#include "TMath.h"
#include "TH2.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TGaxis.h"
#include "TAxis.h"
#include "TBox.h"
#include "TLine.h"
#include "TEfficiency.h"
#include "TRandom3.h"
//std
#include <iostream>
#include <vector>
#include <type_traits>//is_same
#include <string>
#include <memory>//uniqe_ptr
#include <utility>//move
#include <algorithm>//min_element,find_if
#include <numeric>//accumulate
#include <limits>//std::numeric_limits
#include <iterator>//std::distance
#include <chrono>//random seed from time
#include <exception>
#include <cmath>
#include <cstdarg>
//local (IOjuggler)
#include <MessageService.h>

/**
  * @class TH2A
  * @version 0.5
  * @author Marian Stahl
  * @date 2017-05-18
  * @brief 2D adaptive histogram class
  * @details TH2A in principle behaves like a ROOT TH2 class, where some simple functions are implemented @n
  * The class is able to deduce a "common" adaptive binning among several input histograms,
  * which is useful if one wants to take ratios of histograms or has to split
  * the data into several categories, but wants to use the same binning everywhere.@n
  * Tutorial scripts in @ref src/ show how to use the adaptive binning
  * @todo
  *  - see https://gitlab.cern.ch/mstahl/2DAdaptiveBinning/issues
  */
class TH2A : public TNamed {

public:
  //some aliases
  template<typename T> using Matrix = std::vector<std::vector<T> >;
  template<typename S> using EnableIfTStringConvertible = typename std::enable_if<std::is_convertible<S,TString>::value, void>::type*;
  template<typename C> using EnableIfEColorConvertible = typename std::enable_if<std::is_convertible<typename std::decay<C>::type,typename std::underlying_type<EColor>::type >::value, void>::type*;
  template<typename SH> using EnableIfshortConvertible = typename std::enable_if<std::is_convertible<SH,short>::value, void>::type*;
  template<typename H> using EnableIfTH2Ancestor = typename std::enable_if<std::is_base_of<TH2,typename std::decay<H>::type >::value>::type*;
  template<typename F> using EnableIfFloatingPoint = typename std::enable_if<std::is_floating_point<typename std::decay<F>::type>::value, void>::type*;
  //use numeric_limits instead of is_integral to also cover unsigned types
  template<typename I> using EnableIfInteger = typename std::enable_if<std::numeric_limits<typename std::decay<I>::type>::is_integer, void>::type*;

  /**
   * @class ABin
   * @brief Struct with binedges and bincontent + upper and lower uncertainty
   * @details Struct with binedges and bincontent + upper and lower uncertainty
   */
  struct ABin{
    double xlo;
    double xhi;
    double ylo;
    double yhi;
    double content;
    double error_lo;
    double error_hi;
  };

  //begin ctor, dtor and operators

  /**
   * @fn TH2A()
   * @brief Empty constructor
   * @details Empty constructor
   */
  TH2A(){m_msgsvc.debugmsg("called empty constructor");}

  /**
   * @fn TH2A(STR&& name, EnableIfTStringConvertible<STR> = nullptr )
   * @param name: name of file with input binedges
   * @brief Constructor from existing binning
   */
  //second prameter: restrict template to types castable to TString, see
  //http://stackoverflow.com/questions/23762224/check-if-primitive-types-are-castable-in-c
  //and http://stackoverflow.com/questions/16986277/template-argument-type-deduction-fails-with-c11-type-traits
  template <typename STR> TH2A(STR&& name, EnableIfTStringConvertible<STR> = nullptr ) {
    m_msgsvc.debugmsg("called constructor from file");
    std::ifstream ifile;
    ifile.open(static_cast<TString>(name).Data());
    if(!ifile){
      m_msgsvc.errormsg("Can't open input file " + static_cast<TString>(name));
      return;
    }
    //underflow
    m_Binning.emplace_back( ABin{-std::nan(""),-std::nan(""),-std::nan(""),-std::nan(""),0.0,0.0,0.0} );
    //rest of the bins
    double tmp_xlo,tmp_xhi,tmp_ylo,tmp_yhi;
    while(ifile >> tmp_xlo >> tmp_xhi >> tmp_ylo >> tmp_yhi)
      m_Binning.emplace_back( ABin{tmp_xlo,tmp_xhi,tmp_ylo,tmp_yhi,0.0,0.0,0.0} );
    ifile.close();
    //overflow
    m_Binning.emplace_back( ABin{std::nan(""),std::nan(""),std::nan(""),std::nan(""),0.0,0.0,0.0} );
    set_axis_ranges(GetMinX(),GetMaxX(),GetMinY(),GetMaxY());
  }

  /**
   * @fn TH2A(Matrix<T>& binning_matrix)
   * @param binning_matrix: nbins * 4 matrix containing the binning
   * @brief Constructor from existing binning
   * @details Constructor from existing binning given as matrix. The matrix includes under- and overflow bins
   */
  template<typename T = double> TH2A(Matrix<T>& binning_matrix){
    m_msgsvc.debugmsg("called constructor from binning matrix");
    SetBinning(std::forward< Matrix<T> >(binning_matrix));
    set_axis_ranges(GetMinX(),GetMaxX(),GetMinY(),GetMaxY());
  }

  /**
   * @fn ~TH2A()
   * @brief Default overridden destructor
   * @details Default overridden destructor
   */
  virtual ~TH2A() override = default;


  /**
   * @fn TH2A(const TH2A& arg)
   * @param arg: TH2A to copy binning from
   * @brief Copy constructor. Copies only the binning!
   * @note Copies only the binning!
   */
  TH2A(const TH2A& arg) : TNamed(arg) {
    m_msgsvc.debugmsg("called copy constructor");
    this->SetBinning(arg.GetBinning());
    set_axis_ranges(GetMinX(),GetMaxX(),GetMinY(),GetMaxY());
  }

  /**
   * @fn TH2A(TH2A&& )
   * @brief Default move constructor
   * @details Default move constructor
   */
  TH2A(TH2A&& ) = default;

  /**
   * @fn operator=(const TH2A& arg)
   * @param arg: TH2A to copy binning from
   * @brief Copy assignment operator. Copies only the binning!
   * @note Copies only the binning!
   */
  TH2A& operator=(const TH2A& arg){
    m_msgsvc.debugmsg("called copy assignment operator");
    this->SetBinning(arg.GetBinning());
    set_axis_ranges(GetMinX(),GetMaxX(),GetMinY(),GetMaxY());
    return *this;
  }

  /**
   * @fn operator=(TH2A&& )
   * @brief Default move assignment operator
   * @details Default move assignment operator
   */
  TH2A& operator=(TH2A&& ) = default;


  /**
   * @fn operator+(const TH2A& summand) const
   * @param summand: TH2A that is added to the calling TH2A
   * @brief Add two histograms which remain untouched. The result is returned in a new TH2A
   */
  TH2A operator+(const TH2A& summand) const {
    TH2A return_hist = summand;//copies binning
    return_hist.Add(*this,summand);//fills binning of return_hist
    return return_hist;
  }

  /**
   * @fn operator+=(const TH2A& summand)
   * @param summand: TH2A that is added to the calling TH2A
   * @brief Add histogram to calling TH2A
   */
  TH2A operator+=(const TH2A& summand) {
    this->Add(summand);
    return *this;
  }

  /**
   * @fn operator-(const TH2A& subtrahend) const
   * @param subtrahend: TH2A that is subtracted from the calling TH2A
   * @brief Subtract histograms which remain untouched. The result is returned in a new TH2A
   */
  TH2A operator-(const TH2A& subtrahend) const {
    TH2A return_hist = subtrahend;
    return_hist.Add(*this,subtrahend,1.0,-1.0);
    return return_hist;
  }

  /**
   * @fn operator-=(const TH2A& subtrahend)
   * @param subtrahend: TH2A that is subtracted from the calling TH2A
   * @brief Subtract histogram from calling TH2A
   */
  TH2A operator-=(const TH2A& subtrahend) {
    this->Add(subtrahend,-1.0);
    return *this;
  }

  /**
   * @fn operator*(const TH2A& factor) const
   * @param factor: TH2A that multiplied to the calling TH2A
   * @brief Multiply histograms which remain untouched. The result is returned in a new TH2A
   */
  TH2A operator*(const TH2A& factor) const {
    TH2A return_hist = factor;
    return_hist.Multiply(*this,factor);
    return return_hist;
  }

  /**
   * @fn operator*=(const TH2A& factor)
   * @param factor: TH2A that multiplied to the calling TH2A
   * @brief Multiply histogram to calling TH2A
   */
  TH2A operator*=(const TH2A& factor) {
    this->Multiply(factor);
    return *this;
  }

  //end ctor, dtor and operators
  //begin member functions

  /**
   * @fn Add(const TH2A& summand, FL&& weight = 1.0, EnableIfFloatingPoint<FL> = nullptr)
   * @param summand: TH2A that is added to the calling TH2A
   * @param weight: constant scaling factor for \c summand
   * @brief Adds \c summand to calling TH2A
   * @details Adds \c summand to calling TH2A. Errors are added in quadrature. Under- and overflow are manipulated as well
   * @return bool returning true if operation was sucessful
   * @exception runtime_error if calling TH2A and \c summand do not have the same binning
   */
  template <typename FL = double>
  bool Add(const TH2A& summand, FL&& weight = 1.0, EnableIfFloatingPoint<FL> = nullptr){
    if(!this->HasSameBinning(summand))
      throw std::runtime_error("TH2A::Add: binning of input histogram does not match to this binning");
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++){
      m_Binning[ibin].content += weight * summand.GetBinContent(ibin);
      m_Binning[ibin].error_lo = sqrt(this->GetBinErrorLow(ibin)*this->GetBinErrorLow(ibin)
                                      + weight*weight * summand.GetBinErrorLow(ibin)*summand.GetBinErrorLow(ibin));
      m_Binning[ibin].error_hi = sqrt(this->GetBinErrorUp(ibin)*this->GetBinErrorUp(ibin)
                                      + weight*weight * summand.GetBinErrorUp(ibin)*summand.GetBinErrorUp(ibin));
    }
    return true;
  }

  /**
   * @fn Add(const TH2A& s1, const TH2A& s2, FL&& w1 = 1.0, FL&& w2 = 1.0, EnableIfFloatingPoint<FL> = nullptr)
   * @param s1: first summand
   * @param s2: second summand
   * @param w1: constant scaling factor for \c s1
   * @param w2: constant scaling factor for \c s2
   * @brief Sets bin contents and errors of calling TH2A according to adding \c s2 to \c s1.
   * @details Sets bin contents and errors of calling TH2A according to adding \c s2 to \c s1. Errors are added in quadrature. Under- and overflow are manipulated as well
   * @return bool returning true if operation was sucessful
   * @exception runtime_error if \c s1 and \c s2 do not have the same binning
   */
  template <typename FL = double>
  bool Add(const TH2A& s1, const TH2A& s2, FL&& w1 = 1.0, FL&& w2 = 1.0, EnableIfFloatingPoint<FL> = nullptr){
    if(!s1.HasSameBinning(s2))
      throw std::runtime_error("TH2A::Add: binning of input histograms do not match");
    //in case the calling TH2A is empty or has different Binning
    if(!this->HasSameBinning(s1)){
      this->SetBinning(s1.GetBinning());
      m_msgsvc.infomsg("Copied binning from summand");
    }
    for(unsigned int ibin = 0; ibin <= GetNBins()+2; ibin++){
      m_Binning[ibin].content = w1 * s1.GetBinContent(ibin) + w2 * s2.GetBinContent(ibin);
      m_Binning[ibin].error_lo = sqrt(w1*w1 * s1.GetBinErrorLow(ibin)*s1.GetBinErrorLow(ibin)
                                      + w2*w2 * s2.GetBinErrorLow(ibin)*s2.GetBinErrorLow(ibin));
      m_Binning[ibin].error_hi = sqrt(w1*w1 * s1.GetBinErrorUp(ibin)*s1.GetBinErrorUp(ibin)
                                      + w2*w2 * s2.GetBinErrorUp(ibin)*s2.GetBinErrorUp(ibin));
    }
    return true;
  }

  /**
   * @fn BayesianCombine(const TH2A& numerator1, const TH2A& denominator1, const TH2A& numerator2, const TH2A& denominator2)
   * @param numerator1: Numerator (passed) TH2A of first efficiency
   * @param denominator1: Denominator (total) TH2A of first efficiency
   * @param numerator2: Numerator (passed) TH2A of second efficiency
   * @param denominator2: Denominator (total) TH2A of second efficiency
   * @brief Combines two efficiencies. Fills this TH2A with combined efficiency and errors
   * @details See <a href="https://root.cern.ch/doc/master/classTEfficiency.html#aa93dc99f808cf1b73067c6d3e61abd8c">TEfficiency::Combine</a> for details.@n
   * Here, Combine is called in every bin, filling this TH2A with the combined efficiency and the errors obtained from the credibility intervals
   */
  void BayesianCombine(const TH2A& numerator1, const TH2A& denominator1, const TH2A& numerator2, const TH2A& denominator2);

  /**
   * @fn BayesianCombine(const std::vector<TH2A>& numerators, const std::vector<TH2A>& denominators)
   * @param numerators: Numerator (passed) TH2As for the combination
   * @param denominators: Denominator (total) TH2As for the combination
   * @brief Fills this TH2A with combined efficiency and errors
   * @details See <a href="https://root.cern.ch/doc/master/classTEfficiency.html#aa93dc99f808cf1b73067c6d3e61abd8c">TEfficiency::Combine</a> for details.@n
   * Here, Combine is called in every bin, filling this TH2A with the combined efficiency and the errors obtained from the credibility intervals
   * @exception runtime_error if the the number of numerator indices is not equal to the number of denominator indices
   */
  //instead of fighting with variadic templates
  void BayesianCombine(const std::vector<TH2A>& numerators, const std::vector<TH2A>& denominators);

  /**
   * @fn BinomialDivide(const TH2A& numerator, const TH2A& denominator, const bool fix_eff = false)
   * @param numerator: passed histo
   * @param denominator: total histo
   * @param fix_eff: set number of passed events in a bin to the number of total events (or 0) if the efficiency exceeds 1 (or is smaller than 0)
   *        for the computation of the efficiency and it's uncertainty. Default: \c false
   * @brief fills calling TH2A with the efficiency and it's uncertainty
   * @details The calling TH2A does not need to have a binning yet. It will be copied from the numerator TH2A in this case,
   * or if the existing binning is not the same as the one from the inputs.
   * @warning Before setting the \c fix_eff flag to true it is recommended to examine if the unphysical efficiencies are due to fluctuations.
   * When this flag is off (default), warnings will be printed in case of unphysical events. If the status returned by this method is 3, it is recommended to examine the input histograms.
   * @return integer indicating @n
   *  - 0: if the efficiency including uncertainties is well defined (eff-unc > 0 && eff+unc < 1) in every bin
   *  - 1: if the efficiency is well defined in every bin (0 <= eff <= 1), but is out of bounds including uncertainties
   *  - 2: if efficiencies > 1 are compatible with statistical fluctuations at ~95 % CL
   *  - 3: if none of the above cases applies
   * @exception @n
   *  - range_error if the statistics option is not in valid range (0-7 or in TEfficiency namespace: kFCP, kFNormal, kFWilson, kFAC, kFFC, kBJeffrey, kBUniform, kBBayesian)
   *  - runtime errors if binnings do not match
   */
  int BinomialDivide(const TH2A& numerator, const TH2A& denominator, const bool fix_eff = false);

  /**
   * @fn Draw(Option_t* option = "")
   * @param option: Currently only the default empty string is implemented. It is the same as \c colz or \c COLZ
   * @brief Overridden drawing function
   */
  void Draw(Option_t* option = "") override;

  /**
   * @fn void DrawDPBoundary(const char* mother, const char* dau1, const char* dau2, const char* dau3, const int& nbins = 2000) const
   * @param mother: Particle name of mother as defined in $ROOTSYS/etc/pdg_table.txt
   * @param dau1: Particle name of first daughter
   * @param dau2: Particle name of second daughter
   * @param dau3: Particle name of third daughter
   * @param nbins: Number of pixels with which the Dalitz plot boundary is computed
   * @brief Draw Dalitz plot boundary, and shade outside region gray
   */
  void DrawDPBoundary(const char* mother, const char* dau1, const char* dau2, const char* dau3, const int& nbins = 2000) const;
  void DrawDPBoundary(std::string mother, std::string dau1, std::string dau2, std::string dau3, const int& nbins) const;

  /**
   * @fn template <typename F>
  void DrawDPBoundary(const F& m_mother, const F& m_dau1, const F& m_dau2, const F& m_dau3, const int& nbins = 2000,
                      const F& offsetX = 0.0, const F& offsetY = 0.0) const
   * @param m_mother: Mass of mother
   * @param m_d1: Mass of first daughter
   * @param m_d2: Mass of second daughter
   * @param m_d3: Mass of third daughter
   * @param offsetX: Shift the boundary in x-direction
   * @param offsetY: Shift the boundary in y-direction (note: the shifts should be avoided if possible.
   *  partially reconstructed shapes can be emulated by using lower mother masses.
   *  resolution effects can be emulated by scaling down the mass of the daughter that is used on both axes)
   * @param nbins: Number of pixels with which the Dalitz plot boundary is computed
   * @brief Draw Dalitz plot boundary, and shade outside region gray
   */
  template <typename F>
  void DrawDPBoundary(const F& m_mother, const F& m_dau1, const F& m_dau2, const F& m_dau3, const int& nbins = 2000,
                      const F& offsetX = 0.0, const F& offsetY = 0.0) const;
  void DrawSymmetricDPBoundary(double&& M, double&& m_sym, double&& m_other, const int& topology, const int& nbins ) const;

  /**
   * @fn Fill(FL&& x, FL&& y, DL&& w = 1, EnableIfFloatingPoint<FL> = nullptr, EnableIfFloatingPoint<DL> = nullptr)
   * @param x: first coordinate
   * @param y: second coordinate
   * @param w: weight of the event
   * @brief Fills the histogram with a single event at position \c (x,y) with weight \c w
   */
  template <typename FL = double, typename DL = double>
  void Fill(FL&& x, FL&& y, DL&& w = 1, EnableIfFloatingPoint<FL> = nullptr, EnableIfFloatingPoint<DL> = nullptr){
    m_Binning[FindBin(std::forward<FL>(x),std::forward<FL>(y))].content += w;
  }

  /**
   * @fn FillFromHist(TH2X&& hist, EnableIfTH2Ancestor<TH2X> = nullptr)
   * @param hist: fine-binned TH2 histogram
   * @brief Fills a TH2A from a TH2 input histogram
   * @details Fills a TH2A from a TH2 input histogram.
   * The input histogram should be fine-binned to reduce bin-migration errors
   */
  template <typename TH2X = TH2D>
  void FillFromHist(TH2X&& hist, EnableIfTH2Ancestor<TH2X> = nullptr){
    m_msgsvc.debugmsg(TString::Format("Filling TH2A from histogram %s",hist.GetName()));
    for(int xbin = 1; xbin <= hist.GetNbinsX(); xbin++)
      for(int ybin = 1; ybin <= hist.GetNbinsY(); ybin++)
        this->Fill(hist.GetXaxis()->GetBinCenter(xbin),hist.GetYaxis()->GetBinCenter(ybin),hist.GetBinContent(xbin,ybin));
  }

  /**
   * @fn FillRandom(STR&& fname, IL&& ntimes,
                  const unsigned int seed = static_cast<unsigned int>(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())),
                  EnableIfTStringConvertible<STR> = nullptr, EnableIfInteger<IL> = nullptr)
   * @param fname: name of pre-defined 2D function
   * @param ntimes: number of events to be simulated
   * @param seed: seed for TRandom
   * @brief Fills a TH2A from a TF2 in the global function list
   * @details Fills a TH2A from a TF2 in the global function list. Check @ref src/ab02.cpp for reference.
   * @exception @n
   *  - runtime_error if function is not in global list of function or can't be casted to TF2*
   *  - runtime_error if integral over sampling function is less than 0
   */
  template <typename STR, typename IL = int>
  void FillRandom(STR&& fname, IL&& ntimes,
                  const unsigned int seed = static_cast<unsigned int>(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())),
                  EnableIfTStringConvertible<STR> = nullptr, EnableIfInteger<IL> = nullptr){

    //*-*- Search for fname in the list of ROOT defined functions
    TF2* f1 = static_cast<TF2*>(gROOT->GetFunction(static_cast<TString>(fname).Data()));
    if (!f1)
      throw std::runtime_error(TString::Format("TH2A::FillRandom: Unknown function: "
                                               "%s or function doesn't inherit from TF2",
                                               static_cast<TString>(fname).Data()).Data());

    std::vector<double> integral(this->GetNBins(),0.0);
    auto integral_value = 0.f;
    for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
      integral_value += f1->Integral(m_Binning[ibin].xlo, m_Binning[ibin].xhi, m_Binning[ibin].ylo, m_Binning[ibin].yhi);
      integral[ibin] = integral_value;
    }

    // Normalize integral to 1
    if(integral_value <= 0 )
      throw std::runtime_error("TH2A::FillRandom: Integral of sampling function is leq 0");

    for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++)
      integral[ibin] /= integral_value;

    // Start main loop ntimes
    TRandom3 rnd(seed);
    for(IL loop = 0; loop < ntimes; loop++){
      const auto randomnumber = rnd.Rndm();//doesn't matter what the argument of rnd.Rndm() is...
      const unsigned int index = std::distance(integral.begin(),
                                               std::min_element(integral.begin(),integral.end(),
                                                                [&randomnumber](double a, double b){return (a - randomnumber)*(b - randomnumber) <= 0;}));
      m_Binning[index].content += 1;
    }
  }

  /**
   * @fn FindBin(FL&& x, FL&& y, EnableIfFloatingPoint<FL> = nullptr) const
   * @param x: first coordinate
   * @param y: second coordinate
   * @brief Get bin number of given input coordinates
   * @return Bin number as integer
   * @details Returns bin number of given input coordinates
   * Under- or Overflow bins are returned if coordinates are out of range.
   * If both coordinates are out of range, the x-coordinate decides whether the input is under-or overflow
   */
  template <typename FL>
  inline unsigned int FindBin(FL&& x, FL&& y, EnableIfFloatingPoint<FL> = nullptr) const {
    const double X = static_cast<const double>(x), Y = static_cast<const double>(y);
    unsigned int binnumber = std::find_if(m_Binning.begin(),m_Binning.end(),
                                          [&X,&Y](const ABin& bin){return bin.xlo <= X && X < bin.xhi && bin.ylo <= Y && Y < bin.yhi;}) - m_Binning.begin();
    //set binnumber to underflow if X or (Y is underflow and X is not overflow at the same time)
    if(binnumber == GetNBins()+2 && (X < GetMinX() || (Y < GetMinY() && !(X > GetMaxX()))))
      binnumber = 0;
    return binnumber;
  }

  /**
   * @fn GetBinContent(IL&& bin, EnableIfInteger<IL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get bin content of given bin number
   * @return bin content
   */
  template <typename IL>
  inline double GetBinContent(IL&& bin, EnableIfInteger<IL> = nullptr) const {
    return m_Binning[static_cast<unsigned int>(bin)].content;
  }

  /**
   * @fn GetBinError(IL&& bin, EnableIfInteger<IL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get bin error of given bin number
   * @return bin error
   * @details if there is an asymmetric error associated to the bin, this method returns the simple average
   */
  template <typename IL>
  inline double GetBinError(IL&& bin, EnableIfInteger<IL> = nullptr) const {
    //TH2A::Fill doesn't set errors. provide Poissionian errors in that case
    if(m_Binning[static_cast<unsigned int>(bin)].content != 0.f
       && m_Binning[static_cast<unsigned int>(bin)].error_lo == 0.f
       && m_Binning[static_cast<unsigned int>(bin)].error_hi == 0.f){
      m_Binning[static_cast<unsigned int>(bin)].error_lo = sqrt(m_Binning[static_cast<unsigned int>(bin)].content);
      m_Binning[static_cast<unsigned int>(bin)].error_hi = m_Binning[static_cast<unsigned int>(bin)].error_lo;
    }
    return 0.5*(m_Binning[static_cast<unsigned int>(bin)].error_lo+m_Binning[static_cast<unsigned int>(bin)].error_hi);
  }

  /**
   * @fn GetBinErrorLow(IL&& bin, EnableIfInteger<IL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get lower bin error of given bin number
   * @return lower bin error
   */
  template <typename IL>
  inline double GetBinErrorLow(IL&& bin, EnableIfInteger<IL> = nullptr) const {
    //TH2A::Fill doesn't set errors. provide asymptotic Poissionian errors in that case. check both errors nevertheless, we could be in an efficiency bin with eff = 0 or 1
    if(m_Binning[static_cast<unsigned int>(bin)].content != 0.f
       && m_Binning[static_cast<unsigned int>(bin)].error_lo == 0.f
       && m_Binning[static_cast<unsigned int>(bin)].error_hi == 0.f)
      m_Binning[static_cast<unsigned int>(bin)].error_lo = sqrt(m_Binning[static_cast<unsigned int>(bin)].content);
    return m_Binning[static_cast<unsigned int>(bin)].error_lo;
  }

  /**
   * @fn GetBinErrorUp(IL&& bin, EnableIfInteger<IL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get upper bin error of given bin number
   * @return upper bin error
   */
  template <typename IL>
  inline double GetBinErrorUp (IL&& bin, EnableIfInteger<IL> = nullptr) const {
    //TH2A::Fill doesn't set errors. provide asymptotic Poissionian errors in that case. check both errors nevertheless, we could be in an efficiency bin with eff = 0 or 1
    if(m_Binning[static_cast<unsigned int>(bin)].content != 0.f && m_Binning[static_cast<unsigned int>(bin)].error_lo == 0.f && m_Binning[static_cast<unsigned int>(bin)].error_hi == 0.f)
      m_Binning[static_cast<unsigned int>(bin)].error_hi = sqrt(m_Binning[static_cast<unsigned int>(bin)].content);
    return m_Binning[static_cast<unsigned int>(bin)].error_hi;
  }

  /**
   * @fn GetBinLowEdgeX(UIL&& bin, EnableIfInteger<UIL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get lower bin edge of given bin number
   * @return lower bin edge
   */
  template <typename UIL>
  inline double GetBinLowEdgeX(UIL&& bin, EnableIfInteger<UIL> = nullptr) const {
    return m_Binning[static_cast<unsigned int>(bin)].xlo;
  }

  /**
   * @fn GetBinLowEdgeY(UIL&& bin, EnableIfInteger<UIL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get lower bin edge of given bin number
   * @return lower bin edge
   */
  template <typename UIL>
  inline double GetBinLowEdgeY(UIL&& bin, EnableIfInteger<UIL> = nullptr) const {
    return m_Binning[static_cast<unsigned int>(bin)].ylo;
  }

  /**
   * @fn GetBinning()
   * @brief Gets binedges of this TH2A and returns it as Matrix<double>
   * @details Gets binedges of this TH2A and returns it as Matrix<double> including under- and overflow bins
   */
  Matrix<double> GetBinning() const {
    Matrix<double> binning;
    for (const auto& bin : m_Binning)
      binning.emplace_back(std::vector<double>{bin.xlo,bin.xhi,bin.ylo,bin.yhi});
    return binning;
  }

  /**
   * @fn GetBinUpEdgeX(UIL&& bin, EnableIfInteger<UIL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get upper bin edge of given bin number
   * @return upper bin edge
   */
  template <typename UIL>
  inline double GetBinUpEdgeX (UIL&& bin, EnableIfInteger<UIL> = nullptr) const {
    return m_Binning[static_cast<unsigned int>(bin)].xhi;
  }

  /**
   * @fn GetBinUpEdgeY(UIL&& bin, EnableIfInteger<UIL> = nullptr) const
   * @param bin: global binnumber
   * @brief Get upper bin edge of given bin number
   * @return upper bin edge
   */
  template <typename UIL>
  inline double GetBinUpEdgeY (UIL&& bin, EnableIfInteger<UIL> = nullptr) const {
    return m_Binning[static_cast<unsigned int>(bin)].yhi;
  }

  /**
   * @fn GetDimension () const
   * @brief Gets number of dimensions
   * @return currently always 2
   * @details if this class ever inherits from TH1, this becomes obsolete and one just needs to set fDimension
   */
  inline int GetDimension () const { return 2; }

  /**
   * @fn GetMaxX() const
   * @brief Get maximum x from binning
   * @return maximal x
   * @details Get maximum x from binning. In contrast to TH2A::GetXMax() const which returns the limit of the axis
   */
  inline double GetMaxX() const {
    double maximum = std::numeric_limits<double>::min();
    for(auto bin : m_Binning)
      if(bin.xhi > maximum)
        maximum = bin.xhi;
    return maximum;
  }

  /**
   * @fn GetMaxY() const
   * @brief Get maximum y from binning
   * @return maximal y
   * @details Get maximum y from binning. In contrast to TH2A::GetXMax() const which returns the limit of the axis
   */
  inline double GetMaxY() const {
    double maximum = std::numeric_limits<double>::min();
    for(auto bin : m_Binning)
      if(bin.yhi > maximum)
        maximum = bin.yhi;
    return maximum;
  }

  /**
   * @fn GetMaxZ() const
   * @brief Get maximum z
   * @return maximal z
   * @details Get maximum z from finding maximal element in bin contents. In contrast to TH2A::GetZMax() const which returns the limit of the axis
   */
  double GetMaxZ() const {
    return (*std::max_element(m_Binning.begin()+1,m_Binning.end()-1,[](ABin a, ABin b){return a.content < b.content;})).content;
  }

  /**
   * @fn GetMinX() const
   * @brief Get minimum x from binning
   * @return minimal x
   * @details Get minimum x from binning. In contrast to TH2A::GetXMin() const which returns the limit of the axis
   */
  inline double GetMinX() const {
    double minimum = std::numeric_limits<double>::max();
    for(auto bin : m_Binning)
      if(bin.xlo < minimum)
        minimum = bin.xlo;
    return minimum;
  }

  /**
   * @fn GetMinY() const
   * @brief Get minimum y from binning
   * @return minimal y
   * @details Get minimum y from binning. In contrast to TH2A::GetYMin() const which returns the limit of the axis
   */
  inline double GetMinY() const {
    double minimum = std::numeric_limits<double>::max();
    for(auto bin : m_Binning)
      if(bin.ylo < minimum)
        minimum = bin.ylo;
    return minimum;
  }

  /**
   * @fn GetMinZ() const
   * @brief Get minimum z
   * @return minimal z
   * @details Get minimum z from finding minimal element in bin contents. In contrast to TH2A::GetZMin() const which returns the limit of the axis
   */
  double GetMinZ() const {
    return (*std::min_element(m_Binning.begin()+1,m_Binning.end()-1,[](ABin a, ABin b){return a.content < b.content;})).content;
  }

  /**
   * @fn GetNBins() const
   * @brief Gets number of bins excl. U/O
   * @return number of bins excl. U/O bin as unsigned int
   * @details Gets number of bins excluding under- and overflow bins   *
   */
  inline unsigned int GetNBins() const { return m_Binning.size() - 2; }

  /**
   * @fn GetNcells() const
   * @brief Gets number of bins incl. U/O
   * @return number of bins incl. U/O bin as int
   * @details Gets number of bins including under- and overflow bins. To be consistent with TH1. Note that TH2A only has 2 under- and overflow bins, contrary to the TH1 classes
   */
  inline int GetNcells() const { return static_cast<int>(m_Binning.size()); }

  /**
   * @fn GetSumOfWeights() const
   * @brief Return the sum of weights excluding under/overflows.
   */
  inline double GetSumOfWeights() const {
    return std::accumulate(m_Binning.begin()+1,m_Binning.end()-1,0.,[](double sum, ABin bin){return sum + bin.content;});
  }

  /**
   * @fn GetWMean() const
   * @brief get weighted mean of contents in histogram (relies on arithmetic mean of upper and lower errors)
   * @return weighted mean of contents in histogram
   * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8)
   */
  double GetWMean() const {
    double x_bar = 0.0;
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++){
      auto err = m_Binning[ibin].error_lo + m_Binning[ibin].error_hi;
      x_bar += err > 0 ? m_Binning[ibin].content/std::pow(err,2) : 0.0;
    }
    return x_bar*std::pow(GetdWMean(),2);
  }

  /**
   * @fn double GetdWMean() const
   * @brief get uncertainty of weighted mean of contents in histogram (relies on arithmetic mean of upper and lower errors)
   * @return uncertainty of weighted mean of contents in histogram
   * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8)
   */
  double GetdWMean() const {
    double w = 0.0;
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++)
      w += (m_Binning[ibin].error_lo + m_Binning[ibin].error_hi) > 0 ? 1/std::pow(m_Binning[ibin].error_lo + m_Binning[ibin].error_hi,2) : 0.0;
    return 1/std::sqrt(w);
  }

  /**
   * @fn double GetdWMeanLo() const
   * @brief get "lower uncertainty" of weighted mean of contents in histogram
   * @return uncertainty of weighted mean of contents in histogram
   * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8)
   */
  double GetdWMeanLo() const {
    double w = 0.0;
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++)
      w += m_Binning[ibin].error_lo > 0 ? 1/std::pow(m_Binning[ibin].error_lo,2) : 0.0;
    return 1/std::sqrt(w);
  }

  /**
   * @fn double GetdWMeanUp() const
   * @brief get "upper uncertainty" of weighted mean of contents in histogram
   * @return uncertainty of weighted mean of contents in histogram
   * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8)
   */
  double GetdWMeanUp() const {
    double w = 0.0;
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++)
      w += m_Binning[ibin].error_hi > 0 ? 1/std::pow(m_Binning[ibin].error_hi,2) : 0.0;
    return 1/std::sqrt(w);
  }


  /**
   * @fn GetXaxisTitle() const
   * @brief Get axis title
   * @return Axis title as TString
   * @details Get x axis title
   */
  inline TString GetXaxisTitle() const {return m_XAxisTitle; }
  /**
   * @fn GetYaxisTitle() const
   * @brief Get axis title
   * @return Axis title as TString
   * @details Get y axis title
   */
  inline TString GetYaxisTitle() const {return m_YAxisTitle; }

  /**
   * @fn GetXMax() const
   * @brief Get maximum x from axis
   * @return maximal x
   * @details Get maximum x from axis. In contrast to GetMaxX() const which returns limit from the binning
   */
  double GetXMax() const { return m_XHi; }

  /**
   * @fn GetYMax() const
   * @brief Get maximum y from axis
   * @return maximal y
   * @details Get maximum y from axis. In contrast to GetMaxY() const which returns limit from the binning
   */
  double GetYMax() const { return m_YHi; }

  /**
   * @fn GetZMax() const
   * @brief Get maximum z from axis
   * @return maximal z
   * @details Get maximum z from axis. In contrast to GetMaxZ() const which returns limit from the binning
   */
  double GetZMax() const { return m_ZHi; }

  /**
   * @fn GetXMin() const
   * @brief Get minimum x from axis
   * @return minimal x
   * @details Get minimum x from axis. In contrast to GetMinX() const which returns limit from the binning
   */
  double GetXMin() const { return m_XLo; }

  /**
   * @fn GetYMin() const
   * @brief Get minimum y from axis
   * @return minimal y
   * @details Get minimum y from axis. In contrast to GetMinY() const which returns limit from the binning
   */
  double GetYMin() const { return m_YLo; }

  /**
   * @fn GetZMin() const
   * @brief Get minimum z from axis
   * @return minimal z
   * @details Get minimum z from axis. In contrast to GetMinZ() const which returns limit from the binning
   */
  double GetZMin() const { return m_ZLo; }

  /**
   * @fn HasSameBinning(const TH2A& other_hist) const
   * @param other_hist: histogram to compare
   * @brief Compares binning of \c this and \c other_hist
   * @return true if binnings match
   */
  bool HasSameBinning(const TH2A& other_hist) const;

  /**
   * @fn HasSameBinning(const std::vector<TH2A>& other_hists) const
   * @param other_hists: vector of histograms to compare
   * @brief Compares binning of \c this and \c other_hists
   * @return true if all binnings match
   */
  bool HasSameBinning(const std::vector<TH2A>& other_hists) const {
    for(const auto& hist : other_hists)
      if(!this->HasSameBinning(hist))
        return false;
    return true;
  }

  /**
   * @fn MakeBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin)
   * @param inputs: vector of input histograms from which the binning is generated
   * @param MinNumberOfEventsPerBin: smallest bin content in a TH2A bin (the algorithm is stopped before falling below this limit)
   * @brief Creates the adaptive binning
   * @details This is the main method of the adaptive binning. See AdaptiveBinning for details
   */
  template < typename TH2X > void MakeBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin);
  /**
   * @fn MakeBinning(const TH2X& input, const double& MinNumberOfEventsPerBin, EnableIfTH2Ancestor<TH2X> = nullptr)
   * @brief same as MakeBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin) with single input histogram
   * @details same as MakeBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin) with single input histogram
   */
  template < typename TH2X > void MakeBinning(const TH2X& input, const double& MinNumberOfEventsPerBin, EnableIfTH2Ancestor<TH2X> = nullptr){
    MakeBinning(std::vector<TH2X>{input},MinNumberOfEventsPerBin);
  }

  /**
   * @fn Weight(const TH2X& origin, const TH2X& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor)
   * @param origin: fine binned histogram to weight
   * @param target: fine binned histogram to weight to
   * @param truncation_factor: remove weights if they are above truncation_factor*(weighted mean of weight distribution). default 25; a factor less or equal 0 will apply truncation.
   * @param MinNumberOfEventsPerBin: smallest bin content in a TH2A bin (the algorithm is stopped before falling below this limit)
   * @brief Creates the adaptive binning and writes normalized weights to this TH2A
   */
  template <typename TH2X> void Weight(const TH2X& origin, const TH2X& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor=25.);

  /**
   * @fn Multiply(const TH2A& factor, FL&& weight = 1.0, EnableIfFloatingPoint<FL> = nullptr)
   * @param factor: TH2A that is multiplied to the calling TH2A
   * @param weight: constant scaling factor for \c factor
   * @brief Multiplies \c factor to calling TH2A
   * @details Multiplies \c factor to calling TH2A. Errors are propagated using simple uncertainty propagation.
   * Under- and overflow are manipulated as well
   * @return bool returning true if operation was sucessful
   * @exception runtime_error if calling TH2A and \c factor do not have the same binning
   */
  template <typename FL = double>
  bool Multiply(const TH2A& factor, FL&& weight = 1.0, EnableIfFloatingPoint<FL> = nullptr){
    if(!this->HasSameBinning(factor))
      throw std::runtime_error("Add: binning of input histogram does not match to this binning");
    for (unsigned int ibin = 0; ibin < m_Binning.size(); ibin++){
      //calculate errors before manipulating bin content
      m_Binning[ibin].error_lo = sqrt(pow(this->GetBinErrorLow(ibin)*weight*factor.GetBinContent(ibin),2)
                                      + pow(factor.GetBinErrorLow(ibin)*weight*this->GetBinContent(ibin),2));
      m_Binning[ibin].error_hi = sqrt(pow(this->GetBinErrorUp(ibin)*weight*factor.GetBinContent(ibin),2)
                                      + pow(factor.GetBinErrorUp(ibin)*weight*this->GetBinContent(ibin),2));
      m_Binning[ibin].content *= weight * factor.GetBinContent(ibin);
    }
    return true;
  }

  /**
   * @fn Multiply(const TH2A& f1, const TH2A& f2, FL&& w1 = 1.0, FL&& w2 = 1.0, EnableIfFloatingPoint<FL> = nullptr)
   * @param f1: first factor
   * @param f2: second factor
   * @param w1: constant scaling factor for \c f1
   * @param w2: constant scaling factor for \c f2
   * @brief Sets bin contents and errors of calling TH2A according to multiplying \c f2 to \c f1
   * @details Sets bin contents and errors of calling TH2A according to multiplying \c f2 to \c f1.
   * Errors are propagated using simple uncertainty propagation. Under- and overflow are manipulated as well
   * @return bool returning true if operation was sucessful
   * @exception runtime_error if \c f1 and \c f2 do not have the same binning
   */
  template <typename FL = double>
  bool Multiply(const TH2A& f1, const TH2A& f2, FL&& w1 = 1.0, FL&& w2 = 1.0, EnableIfFloatingPoint<FL> = nullptr){
    if(!this->HasSameBinning(f1))
      throw std::runtime_error("Add: binning of input histogram does not match to this binning");
    //in case the calling TH2A is empty or has different Binning
    if(!this->HasSameBinning(f1)){
      this->SetBinning(f1.GetBinning());
      m_msgsvc.infomsg("Copied binning");
    }
    for (unsigned int ibin = 0; ibin < m_Binning.size(); ibin++){
      m_Binning[ibin].content = w1*f1.GetBinContent(ibin) * w2*f2.GetBinContent(ibin);
      m_Binning[ibin].error_lo = sqrt(pow(f1.GetBinErrorLow(ibin)*w1*w2*f2.GetBinContent(ibin),2)
                                      + pow(f2.GetBinErrorLow(ibin)*w1*w2*f1.GetBinContent(ibin),2));
      m_Binning[ibin].error_hi = sqrt(pow(f1.GetBinErrorUp(ibin)*w1*w2*f2.GetBinContent(ibin),2)
                                      + pow(f2.GetBinErrorUp(ibin)*w1*w2*f1.GetBinContent(ibin),2));
    }
    return true;
  }


  /**
   * @fn Print(Option_t* option = "")
   * @param option: Currently only the default empty string is implemented
   * @brief Prints bin edges, bin content and errors to stdout
   */
  inline void Print(Option_t* option = "") const override {
    if(static_cast<TString>(option) != "") m_msgsvc.warningmsg(TString::Format("Option %s is not implemented",option));
    auto inital_msglvl = m_msgsvc.GetMsgLvl();
    if(inital_msglvl < MSG_LVL::INFO)m_msgsvc.SetMsgLvl(MSG_LVL::INFO);
    for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
      auto bin = m_Binning[ibin];
      if(bin.error_lo == bin.error_hi)
        m_msgsvc.infomsg(TString::Format("Bin %-4u: %-8.4g  %-8.4g  %-8.4g  %-8.4g  Content: %-8.4g +- %-8.4g",
                                         ibin,bin.xlo,bin.xhi,bin.ylo,bin.yhi,bin.content,bin.error_hi));
      else
        m_msgsvc.infomsg(TString::Format("Bin %-4u: %-8.4g  %-8.4g  %-8.4g  %-8.4g  Content: %-8.4g - %-8.4g + %-8.4g",
                                         ibin,bin.xlo,bin.xhi,bin.ylo,bin.yhi,bin.content,bin.error_lo,bin.error_hi));
    }
    m_msgsvc.infomsg("Underflow: " + std::to_string(m_Binning[0].content) + " Overflow: " + std::to_string(m_Binning[GetNBins()+1].content));
    m_msgsvc.SetMsgLvl(inital_msglvl);
  }

  /**
   * @fn Scale(const T& factor, EnableIfFloatingPoint<T> = nullptr)
   * @param factor: constant factor to scale the whole histogram
   * @brief Scales bin contents and errors of calling TH2A
   * @details Scales bin contents and errors of calling TH2A. Under- and overflow are scaled as well
   */
  template<typename T> inline void Scale(const T& factor, EnableIfFloatingPoint<T> = nullptr){
    for (auto& bin : m_Binning){
      bin.content *= static_cast<double>(factor);
      bin.error_lo *= static_cast<double>(factor);
      bin.error_hi *= static_cast<double>(factor);
    }
  }

  /**
   * @fn SetBinContent(IL&& bin, FL&& content, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr)
   * @param bin: global bin number
   * @param content: new bin content
   * @brief Sets bin content
   */
  template <typename IL, typename FL>
  inline void SetBinContent(IL&& bin, FL&& content, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr){
    m_Binning[static_cast<unsigned int>(bin)].content = static_cast<double>(content);
  }

  /**
   * @fn SetBinError(IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr)
   * @param bin: global bin number
   * @param err: new bin error
   * @brief Sets bin error
   */
  template <typename IL, typename FL>
  inline void SetBinError(IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr){
    m_Binning[static_cast<unsigned int>(bin)].error_lo = static_cast<double>(err);
    m_Binning[static_cast<unsigned int>(bin)].error_hi = static_cast<double>(err);
  }

  /**
   * @fn SetBinErrorLow(IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr)
   * @param bin: global bin number
   * @param err: new bin error
   * @brief Sets lower bin error
   */
  template <typename IL, typename FL>
  inline void SetBinErrorLow(IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr){
    m_Binning[static_cast<unsigned int>(bin)].error_lo = static_cast<double>(err);
  }

  /**
   * @fn SetBinErrorUp(IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr)
   * @param bin: global bin number
   * @param err: new bin error
   * @brief Sets upper bin error
   */
  template <typename IL, typename FL>
  inline void SetBinErrorUp (IL&& bin, FL&& err, EnableIfInteger<IL> = nullptr, EnableIfFloatingPoint<FL> = nullptr){
    m_Binning[static_cast<unsigned int>(bin)].error_hi = static_cast<double>(err);
  }

  /**
   * @fn SetBinning(Matrix<T>&& binning_matrix)   *
   * @param binning_matrix: Binning as a @f$ \texttt{nbins} \times 4 @f$ matrix
   * @brief Sets binedges of this TH2A
   * @brief Sets binedges of this TH2A including under- and overflow bins
   */
  template<typename T = double> void SetBinning(Matrix<T>&& binning_matrix) {
    if(binning_matrix[0].size() != 4u)
      throw std::runtime_error("You were trying to construct a rectangle with more or less than 4 edges");
    for(auto ebin : binning_matrix){
      m_Binning.emplace_back( ABin{static_cast<double>(ebin[0]),static_cast<double>(ebin[1]),static_cast<double>(ebin[2]),static_cast<double>(ebin[3]),0.0,0.0,0.0} );
    }
  }

  /**
   * @fn SetXRange(FL&& lo, FL&& hi)
   * @param lo
   * @param hi
   * @brief Set axis range manually
   */
  template <typename FL>
  inline void SetXRange(FL&& lo, FL&& hi, EnableIfFloatingPoint<FL> = nullptr){
    m_XLo = lo;m_XHi = hi;
  }
  /**
   * @fn SetYRange(FL&& lo, FL&& hi)
   * @param lo
   * @param hi
   * @brief Set axis range manually
   */
  template <typename FL>
  inline void SetYRange(FL&& lo, FL&& hi, EnableIfFloatingPoint<FL> = nullptr){
    m_YLo = lo;m_YHi = hi;
  }
  /**
   * @fn SetZRange(FL&& lo, FL&& hi)
   * @param lo
   * @param hi
   * @brief Set axis range manually
   */
  template <typename FL>
  inline void SetZRange(FL&& lo, FL&& hi, EnableIfFloatingPoint<FL> = nullptr){
    m_ZLo = lo;m_ZHi = hi;
  }

  /**
   * @fn Write(const char *name = 0, Int_t option = 0, Int_t bufsize = 0) override
   * @param name: name of this in file
   * @param option: can be a combination of: \c kSingleKey, \c kOverwrite or \c kWriteDelete
   * @param bufsize: force a given buffer size
   * @brief Writes this object to current directory
   * @details See <a href="https://root.cern.ch/doc/master/classTObject.html#a19782a4717dbfd4857ccd9ffa68aa06d">TObject::Write</a> for details
   */
  Int_t Write(const char *name = 0, Int_t option = 0, Int_t bufsize = 0) override {
    m_ZLo = this->GetMinZ();
    m_ZHi = this->GetMaxZ();
    m_msgsvc.debugmsg(TString::Format("Setting Z range to [%-.6g,%-.6g]",m_ZLo,m_ZHi));
    return TNamed::Write(name,option,bufsize);
  }
  using TNamed::Write;//don't hide TNamed::Write

  /**
   * @fn WriteBinning(STR&& name, EnableIfTStringConvertible<STR> = nullptr)
   * @param name: name of the outputfile
   * @brief Writes the adaptive binning to a ascii output file with name `name`
   */
  template <typename STR> void WriteBinning(STR&& name, EnableIfTStringConvertible<STR> = nullptr) const {
    std::ofstream ofile;
    ofile.open(static_cast<TString>(name).Data());
    if(!ofile){
      m_msgsvc.errormsg("Can't open output file " + static_cast<TString>(name));
      return;
    }
    for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++)
      ofile << m_Binning[ibin].xlo << "\t" << m_Binning[ibin].xhi << "\t"
            << m_Binning[ibin].ylo << "\t" << m_Binning[ibin].yhi << std::endl;
    ofile.close();
    m_msgsvc.infomsg("Binnnig written to " + static_cast<TString>(name));
    return;
  }

  //static functions
  /**
   * @fn SetCL(FL&& cl, EnableIfFloatingPoint<FL> = nullptr)
   * @param cl: Confidence/Credibility level
   * @brief Sets the confidence level (0 < cl < 1) The default value is 0.6827 (1 sigma)
   */
  template <typename FL>
  static inline void SetCL(FL&& cl, EnableIfFloatingPoint<FL> = nullptr) {
    m_ConfidenceLevel = static_cast<double>(cl);
  }

  /**
   * @fn SetLineColor(Col&& lc)
   * @param lc: color as something that can be casted to \c EColor
   * @brief Sets color of bin-border-lines
   */
  template <typename Col = EColor>
  static inline void SetLineColor(Col&& lc, EnableIfEColorConvertible<Col> = nullptr) {
    m_LineColor = static_cast<EColor>(lc);
  }

  /**
   * @fn SetLineWidth(SL&& wd)
   * @param wd: LineWidth as something that can be casted to \c short
   * @brief Sets width of bin-border-lines (set to 0 if none should be drawn)
   */
  template <typename SL>
  static inline void SetLineWidth(SL&& wd, EnableIfshortConvertible<SL> = nullptr) {
    m_LineWidth = static_cast<short>(wd);
  }

  /**
   * @fn SetPriorParams(FL&& alpha,FL&& beta, EnableIfFloatingPoint<FL> = nullptr)
   * @brief Sets the shape parameters of the prior
   * @param alpha: Shape parameter \f$\alpha\f$ (has to be \f$> 0\f$)
   * @param beta: Shape parameter \f$\beta\f$ (has to be \f$> 0\f$)
   * @details The prior probability of the efficiency is given by the beta distribution:
   * \f[
   *   f(\varepsilon;\alpha;\beta) = \frac{1}{B(\alpha,\beta)} \varepsilon^{\alpha-1} (1 - \varepsilon)^{\beta-1}
   * \f]
   */
  template <typename FL> static inline void SetPriorParams(FL&& alpha,FL&& beta, EnableIfFloatingPoint<FL> = nullptr) {
    m_alpha = static_cast<double>(alpha); m_beta = static_cast<double>(beta);
  }

  /**
   * @fn SetStatOpt(StatOpt&& statopt)
   * @param statopt: Statistics option from <a href="https://root.cern.ch/doc/master/classTEfficiency.html#af27fb4e93a1b16ed7a5b593398f86312">TEfficiency::EStatOption</a>
   * as TEfficiency::EStatOption or integral type
   * @brief Sets the inference from which binomial confidence or credibility intervals are calculated
   * @details
   * Options:
   *   - \c kFCP (=0)(default): using the Clopper-Pearson interval (recommended by PDG)
   *   - \c kFNormal (=1) : using the normal approximation
   *   - \c kFWilson (=2) : using the Wilson interval
   *   - \c kFAC (=3) : using the Agresti-Coull interval
   *   - \c kFFC (=4) : using the Feldman-Cousins frequentist method
   *   - \c kBJeffrey (=5) : using the Jeffrey interval, sets prior params alpha = 0.5 and beta = 0.5
   *   - \c kBUniform (=6) : using a uniform prior, sets prior params alpha = 1 and beta = 1
   *   - \c kBBayesian (=7) : using a custom prior defined by parameters set with TH2A::SetPriorParams()
   */
  template <typename StatOpt = TEfficiency::EStatOption> static inline void SetStatOpt(StatOpt&& statopt) {
    m_StatisticsOption = static_cast<TEfficiency::EStatOption>(statopt);
  }

  /**
   * @fn SetVerbosity(int lvl)
   * @param lvl: 0 is ERROR, 1 is WARNING, 2 is INFO, 3 is DEBUG
   * @brief Sets the stdout verbosity
   */
  static void SetVerbosity(int lvl){m_msgsvc.SetMsgLvl(lvl);}

  /**
   * @fn SetXaxisTitle(STR&& title)
   * @param title
   * @brief Sets axis title to \c title
   */
  template <class STR>
  static inline void SetXaxisTitle(STR&& title, EnableIfTStringConvertible<STR> = nullptr){
    m_XAxisTitle = static_cast<TString>(title);
  }
  /**
   * @fn SetYaxisTitle(STR&& title)
   * @param title
   * @brief Sets axis title to \c title
   */
  template <class STR>
  static inline void SetYaxisTitle(STR&& title, EnableIfTStringConvertible<STR> = nullptr){
    m_YAxisTitle = static_cast<TString>(title);
  }


private:
  mutable std::vector<ABin>       m_Binning;
  mutable TH2D                    m_dummy = TH2D();
  int                             m_NDummyBins = 100;
  double                          m_XHi = 0.f;
  double                          m_XLo = 0.f;
  double                          m_YHi = 0.f;
  double                          m_YLo = 0.f;
  double                          m_ZHi = -999.9;
  double                          m_ZLo = 999.9;
  static double                   m_alpha;
  static double                   m_beta;
  static double                   m_ConfidenceLevel;
  static Color_t                  m_LineColor;
  static short                    m_LineWidth;
  static MessageService           m_msgsvc;
  static TEfficiency::EStatOption m_StatisticsOption;
  static TString                  m_XAxisTitle;
  static TString                  m_YAxisTitle;

  /**
   * @fn set_axis_ranges(TH2X& hist, EnableIfTH2Ancestor<TH2X> = nullptr)
   * @brief Simple function to set the range of the TH2A's axes from an input histogram
   */
  template<typename TH2X>
  inline void set_axis_ranges(TH2X& hist, EnableIfTH2Ancestor<TH2X> = nullptr){
    m_XLo = hist.GetXaxis()->GetXmin();
    m_XHi = hist.GetXaxis()->GetXmax();
    m_YLo = hist.GetYaxis()->GetXmin();
    m_YHi = hist.GetYaxis()->GetXmax();
    m_msgsvc.debugmsg(TString::Format("Axis ranges: %.6g, %.6g, %.6g, %.6g",m_XLo,m_XHi,m_YLo,m_YHi));
  }

  /**
   * @fn set_axis_ranges(FL&& xlo, FL&& xhi, FL&& ylo, FL&& yhi, EnableIfFloatingPoint<FL> = nullptr)
   * @param xlo: lower limit of x axis (not binning)
   * @param xhi: upper limit of x axis (not binning)
   * @param ylo: lower limit of y axis (not binning)
   * @param yhi: upper limit of y axis (not binning)
   * @brief Set the range of the TH2A's axes
   */
  template<typename FL>
  inline void set_axis_ranges(FL&& xlo, FL&& xhi, FL&& ylo, FL&& yhi, EnableIfFloatingPoint<FL> = nullptr){
    m_XLo = xlo;
    m_XHi = xhi;
    m_YLo = ylo;
    m_YHi = yhi;
    m_msgsvc.debugmsg(TString::Format("Axis ranges: %.6g, %.6g, %.6g, %.6g",m_XLo,m_XHi,m_YLo,m_YHi));
  }

  /**
   * @fn GetEffCI(const double& total, const double& passed, const bool& bUpper)
   * @param total: denominator of efficiency (will be implicitly casted to int from TEfficiency methods)
   * @param passed: numerator of efficiency (will be implicitly casted to int from TEfficiency methods)
   * @param bUpper: returns upper limit of the confidence interval if true (otherwise lower)
   * @brief selects the inference with witch the confidence interval is calculated and returns it's upper or lower limit
   * @exception range_error if the statistics option is not in valid range (0-7 or in TEfficiency namespace: kFCP, kFNormal, kFWilson, kFAC, kFFC, kBJeffrey, kBUniform, kBBayesian)
   */
  double GetEffCI(const double& total, const double& passed, const bool& bUpper) const;

  //needed to embed the class into ROOT
  ClassDefOverride(TH2A,2)
};
#endif
