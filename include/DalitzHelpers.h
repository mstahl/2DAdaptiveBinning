#ifndef DALITZHELPERS_H
#define DALITZHELPERS_H
#include "Rtypes.h"
#include "TGraph.h"
#include "TMath.h"
#include <cmath>
#include "../IOjuggler/CalcGizmo.h"

Double_t lower_DP_boundary(Double_t* x, Double_t* par){
  return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],false),2);
}

Double_t upper_DP_boundary(Double_t* x, Double_t* par){
  return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],true),2);
}

Double_t cut_through_the_DP(Double_t* x, Double_t* par){
  return par[0]+(x[0]-par[2])/(par[3]-par[2])*(par[1]-par[0]);
}

Double_t lower_boundary_so_so(Double_t* x, Double_t* par){
  if(x[0]<par[4])
    return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],false),2);
  else return x[0];
}

Double_t upper_boundary_so_so(Double_t* x, Double_t* par){
  if(x[0]>par[4])
    return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],true),2);
  else return x[0];
}

Double_t lower_boundary_so_ss(Double_t* x, Double_t* par){
  if(x[0]>par[4])
    return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],false),2);
  else {
    std::array<double,1> xx = {x[0]};
    std::array<double,4> p = {pow(par[0]-par[1],2),pow(par[2]+par[3],2),pow(CalcGizmo::pr_endpoint(par[0],par[0]-par[1],par[2],par[3],par[1]),2),par[4]};
    return cut_through_the_DP(xx.data(),p.data());
  }
}

Double_t upper_boundary_so_ss(Double_t* x, Double_t* par){
  if(x[0]<par[4])
    return pow(CalcGizmo::pr_endpoint(par[0],sqrt(x[0]),par[1],par[2],par[3],true),2);
  else {
    std::array<double,1> xx = {x[0]};
    std::array<double,4> p = {pow(par[0]-par[1],2),pow(par[2]+par[3],2),par[4],pow(CalcGizmo::pr_endpoint(par[0],2*par[2],par[2],par[2],par[1]),2)};
    return cut_through_the_DP(xx.data(),p.data());
  }
}


TGraph shade(const TF1& f1, const TF1& f2, const double& xmin, const double& xmax, const double& ymin, const double& ymax,
             const double& xoff = 0.0, const double& yoff = 0.0) {//https://root.cern.ch/phpBB3/viewtopic.php?t=5520

  TGraph gr;
  const auto fmin = f1.GetXmin()+xoff;
  const auto fmax = f1.GetXmax()+xoff;
  auto npx = f1.GetNpx();
  Int_t npoints = 0;
  const auto dx = (fmax-fmin)/npx;
  auto x = xmin;
  while (x < fmin) {
    gr.SetPoint(npoints,x,ymin);
    npoints++;
    x += dx;
  }
  while (x <= fmax) {
    gr.SetPoint(npoints,x,f1.Eval(x-xoff)+yoff);//take offset into account
    npoints++;
    x += dx;
  }
  while (x <= xmax) {
    gr.SetPoint(npoints,x,ymin);
    npoints++;
    x += dx;
  }
  //change direction (we jumped one step too far)
  x -= dx;
  while (x > fmax) {
    gr.SetPoint(npoints,x,ymax);
    npoints++;
    x -= dx;
  }
  while (x >= fmin) {
    gr.SetPoint(npoints,x,f2.Eval(x-xoff)+yoff);
    npoints++;
    x -= dx;
  }
  while (x >= xmin) {
    gr.SetPoint(npoints,x,ymax);
    npoints++;
    x -= dx;
  }
  return gr;
}
#endif
