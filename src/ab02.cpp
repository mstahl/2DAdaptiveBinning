/**
  * @file ab02.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2016-08-19
  * @brief This tutorial shows how to fill a TH2A with efficiencies from TEfficiency and combine 2 efficiencies
  */

#include "TROOT.h"
#include "TCanvas.h"
#include "TF2.h"
#include "TH2D.h"
#include "TFile.h"

#include <utility>

#include "lhcbStyle.h"
#include "TH2A.h"

//function for toy distributions in pt and eta
double KinNorm(double* x,double* par);

int main(int argc, char** argv){

  lhcbStyle();
  TH2A::SetVerbosity(3);

  //define parameters for toy distributions in pt and eta
  const double PTlo = 0.5, PThi = 50;
  const double etalo = 1.6, etahi = 5.4;
  const double N_pT = 1, mpv_pT = 2.5, sigma_pT = 0.6;
  const double N_eta = 1, alpha_eta = -0.6, n_eta = 3.2, mean_eta = 2.9, sigma_eta = 0.3;

  TF2 *kinematics = new TF2("kinematics",KinNorm,PTlo,PThi,etalo,etahi,8);
  double params [8] = {N_pT,mpv_pT,sigma_pT,N_eta,alpha_eta,n_eta,mean_eta,sigma_eta};
  kinematics->SetParameters(params);

  //sample a histogram using random numbers
  TH2D h1("h1","",1000,PTlo,PThi,1000,etalo,etahi);
  h1.FillRandom("kinematics",20000);
  //make another histogram with slightly different parameters
  params[1] = 2.55;
  params[2] = 0.62;
  kinematics->SetParameters(params);
  TH2D h2("h2","",1000,PTlo,PThi,1000,etalo,etahi);
  h2.FillRandom("kinematics",30000);

  //make an adaptive binning which has at least 200 events in every bin of the input histograms h1 and h2
  TH2A h1_adaptive;
  h1_adaptive.MakeBinning(std::vector<TH2D>{h1,h2},200);
  //Fill second histogram (see tutorial ab00)
  TH2A h2_adaptive = h1_adaptive;
  h2_adaptive.FillFromHist(h2);
  //initilize TH2A for efficiency. one could also copy the binning as it was done for h2_adaptive, but BinomialDivide can also take care of this internally
  TH2A eff_h1overh2_adaptive;
  //Set statistics option for Efficiencies (this can also be done globally by calling TH2A::SetStatOpt(TEfficiency::kBBayesian) and TH2A::SetPriorParams(2.0,1.4))
  eff_h1overh2_adaptive.SetStatOpt(TEfficiency::kBBayesian);
  eff_h1overh2_adaptive.SetPriorParams(2.0,1.4);//alpha and beta of the Beta-function that is used as prior
  //Note that Jeffreys prior is considered as least informative ("most objective") prior http://arxiv.org/abs/0908.0130v8
  //it can be set by SetStatOpt(TEfficiency::kBJeffrey) or equivalently by SetStatOpt(TEfficiency::kBBayesian) and SetPriorParams(0.5,0.5)

  //now eff_h1overh2_adaptive gets filled
  eff_h1overh2_adaptive.BinomialDivide(h1_adaptive,h2_adaptive);
  eff_h1overh2_adaptive.Print();

  //create 3 more TH2As from binning of h1_adaptive
  std::vector<TH2A> pass_total_eff_and_combeff(4,h1_adaptive);
  //Fill the first two directly this time
  params[4] = -0.68;
  kinematics->SetParameters(params);
  pass_total_eff_and_combeff[0].FillRandom("kinematics",40000);
  pass_total_eff_and_combeff[0].SetName("pass2");
  params[6] = 2.95;
  kinematics->SetParameters(params);
  pass_total_eff_and_combeff[1].FillRandom("kinematics",70000);
  pass_total_eff_and_combeff[1].SetName("total2");
  pass_total_eff_and_combeff[2].BinomialDivide(pass_total_eff_and_combeff[0],pass_total_eff_and_combeff[1]);
  pass_total_eff_and_combeff[2].SetName("eff2");
  //bayesian combine the efficiencies
  //note that you can also use pass_total_eff_and_combeff[3].BayesianCombine( std::vector<TH2A>{h1_adaptive,pass_total_eff_and_combeff[0]} std::vector<TH2A>{h2_adaptive, pass_total_eff_and_combeff[1]} );
  //the number of passed and total TH2As using the function with vectors can be as high as memory allows. so you can combine an arbitrary number of efficiencies with this method
  pass_total_eff_and_combeff[3].BayesianCombine(h1_adaptive, h2_adaptive, pass_total_eff_and_combeff[0], pass_total_eff_and_combeff[1]);
  pass_total_eff_and_combeff[3].SetName("combeff");

  TCanvas c1("c1","Canvas for adaptive binning",10,10,2048,1280) ;
  gPad->SetTopMargin(0.03);
  gPad->SetBottomMargin(0.15);
  gPad->SetRightMargin(0.13);
  gPad->SetLeftMargin(0.15);

  //set axis labels as static function so they don't need to be set for every single TH2A in scope
  TH2A::SetXaxisTitle("p_{T} (GeV)");
  TH2A::SetYaxisTitle("#eta");
  h1_adaptive.Draw();
  if(!gSystem->OpenDirectory("plots"))gSystem->mkdir("plots");
  c1.SaveAs("plots/ab02_h1_adaptive.pdf");

  c1.Clear();
  h2_adaptive.Draw();
  c1.SaveAs("plots/ab02_h2_adaptive.pdf");

  //set same range for all efficiencies. using template< class T > constexpr T min( std::initializer_list<T> ilist ) here
  auto glob_min = std::min({eff_h1overh2_adaptive.GetMinZ(),pass_total_eff_and_combeff[2].GetMinZ(),pass_total_eff_and_combeff[3].GetMinZ()});
  auto glob_max = std::max({eff_h1overh2_adaptive.GetMaxZ(),pass_total_eff_and_combeff[2].GetMaxZ(),pass_total_eff_and_combeff[3].GetMaxZ()});

  c1.Clear();
  eff_h1overh2_adaptive.SetZRange(glob_min,glob_max);
  eff_h1overh2_adaptive.Draw();
  c1.SaveAs("plots/ab02_eff_h1overh2_adaptive.pdf");

  pass_total_eff_and_combeff[2].SetZRange(glob_min,glob_max);
  pass_total_eff_and_combeff[3].SetZRange(glob_min,glob_max);

  for(auto& adahist : pass_total_eff_and_combeff){
    c1.Clear();
    adahist.Draw();
    c1.SaveAs(TString::Format("plots/ab02_%s.pdf",adahist.GetName()).Data());
  }

  //you can also write TH2As to a file (but they are not added to the file automagically like TH1s in ROOT)
  TFile* ff = TFile::Open("files/myfile.root","RECREATE");
  h1_adaptive.Write("h1_adaptive");
  h2_adaptive.Write("h2_adaptive");
  eff_h1overh2_adaptive.Write("eff_h1overh2_adaptive");
  for(auto& adahist : pass_total_eff_and_combeff)
    adahist.Write(adahist.GetName());
  ff->Close();

  delete kinematics;
  return 0;
}

double KinNorm(double* x,double* par){
    double pT = x[0];
    double eta = x[1];
    //pT kinematic
    double Norm_pT = par[0];
    double mpv = par[1];
    double sigma = par[2];
    //eta kinematic
    double Norm_eta = par[3];
    double alpha = par[4];
    double n = par[5];
    double mean = par[6];
    double sigma_cb = par[7];

    double landau = TMath::Landau(pT,mpv,sigma,true);
    //kinematic distributions in eta
    double t = (eta-mean)/sigma_cb;
    if (alpha < 0) t = -t;
    double absalpha = fabs(alpha);
    double cbpdf;
    if (t >= -absalpha) {
        cbpdf = exp(-0.5*t*t);
    }
    else {
        double a = TMath::Power(n/absalpha,n)*exp(-0.5*absalpha*absalpha);
        double b = n/absalpha - absalpha;
        cbpdf = a/TMath::Power(b - t, n);
    }
    return Norm_pT*landau*Norm_eta*cbpdf;
}
