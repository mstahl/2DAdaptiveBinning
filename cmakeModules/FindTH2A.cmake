#//-------------------------------------------------------------------------
#//
#// Description:
#//      cmake module for finding TH2A installation
#//      TH2A installation location is defined by environment variable $TH2A_DIR
#//
#//      following variables are defined here:
#//      TH2A_FOUND       - flag if everything went smoothly
#//      TH2A_MODULE      - TH2A module
#//      TH2A_INCLUDE_DIR - TH2A header directory (note: TH2A_MODULE uses this and the local version of IOjuggler)
#//      TH2A_LIBRARY_DIR - TH2A library directory
#//
#//      Example usage:
#//          find_package(TH2A REQUIRED)
#//
#// Author List:
#//      Marian Stahl         Heidelberg
#//-------------------------------------------------------------------------


set(TH2A_FOUND FALSE)
set(TH2A_ERROR_REASON "")
set(TH2A_MODULE TH2A CACHE STRING "variable defaulting to 'TH2A'")

if($ENV{TH2A_DIR} STREQUAL "")
  set(TH2A_ERROR_REASON "${TH2A_ERROR_REASON} Environment variable TH2A_DIR is not set.")
else()
  set(TH2A_FOUND TRUE)

  set(TH2A_INCLUDE_DIR "$ENV{TH2A_DIR}/include")
  if(NOT EXISTS "${TH2A_INCLUDE_DIR}")
    set(TH2A_FOUND FALSE)
    set(TH2A_ERROR_REASON "${TH2A_ERROR_REASON} Directory '${TH2A_INCLUDE_DIR}' does not exist.")
  endif()

  set(TH2A_LIBRARY_DIR "${CMAKE_BINARY_DIR}/lib")

  if(NOT ROOT_FOUND)
    message(WARNING "ROOT not found. Trying to find it from here...")
    find_package(ROOT 6.0 REQUIRED)
  endif()

  if(ROOT_FOUND)
    set(CMAKE_INSTALL_LIBDIR ${TH2A_LIBRARY_DIR})
    ROOT_GENERATE_DICTIONARY(G__TH2A $ENV{TH2A_DIR}/include/TH2A.h $ENV{TH2A_DIR}/include/AdaptiveBinning.h MODULE ${TH2A_MODULE} LINKDEF $ENV{TH2A_DIR}/include/TH2A_LinkDef.h
      OPTIONS -I$ENV{TH2A_DIR}/IOjuggler)#add IOjuggler as include directory (do it here by hand rather than global include_directories())
    add_library(${TH2A_MODULE} SHARED $ENV{TH2A_DIR}/src/TH2A.cpp $ENV{TH2A_DIR}/src/AdaptiveBinning.cpp G__TH2A.cxx)
    #defining the include directories as PUBLIC transfers them to any executable using the module
    target_include_directories(${TH2A_MODULE} PUBLIC ${TH2A_INCLUDE_DIR} $ENV{TH2A_DIR}/IOjuggler)
    target_link_libraries(${TH2A_MODULE} -lEG -lRooFitCore -lRooFit -lCore -lGraf -lHist -lRIO)
  else()
    set(TH2A_FOUND FALSE)
    set(TH2A_ERROR_REASON "${TH2A_ERROR_REASON} ROOT was not found.")
  endif()
endif()

# make variables changeable
mark_as_advanced(TH2A_INCLUDE_DIR TH2A_LIBRARY_DIR)

# report result
if(TH2A_FOUND)
  message(STATUS "Found TH2A in '$ENV{TH2A_DIR}'.")
  message(STATUS "Using TH2A include directory '${TH2A_INCLUDE_DIR}'.")
  message(STATUS "Will create dynamic library in '${TH2A_LIBRARY_DIR}' on demand.")
else()
  if(TH2A_FIND_REQUIRED)
    message(FATAL_ERROR "Unable to find requested TH2A installation:${TH2A_ERROR_REASON}")
  else()
    if(NOT TH2A_FIND_QUIETLY)
      message(STATUS "TH2A was not found:${TH2A_ERROR_REASON}")
    endif()
  endif()
endif()
