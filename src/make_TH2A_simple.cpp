/**
  * @file make_TH2A_simple.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2018-09-24
  * @brief This script allows you to create an adaptive binning from a single histogram.
  *        All options are parsed from the command line.
  */

#include "TStopwatch.h"
#include <TH2A.h>
#include <IOjuggler.h>

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "d:i:ho:t:v:","nonoptions: <NEventsPerBin> (default: 100), <new hist name> (default: old+_adaptive)",0);
  MessageService msgsvc("make_TH2A_simple",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  TH2A::SetVerbosity(options.get<int>("verbosity"));
  const auto wd           = options.get<std::string>("workdir");
  const auto histname     = options.get<std::string>("treename");
  auto infile             = IOjuggler::get_file(options.get<std::string>("infilename"),wd);
  TFile* outfile          = TFile::Open((wd+"/"+options.get<std::string>("outfilename")).data(),"RECREATE");

  auto nevpb        = 100.f;
  auto histname_new = histname + "_adaptive";
  if(const auto& ea = options.get_child_optional("extraargs") ){
      nevpb = std::stod(options.get_child("extraargs").front().second.data());
      if((*ea).size() > 1) histname_new = options.get_child("extraargs").back().second.data();
  }

  TH2A h1_adaptive;
  h1_adaptive.MakeBinning(*IOjuggler::get_obj<TH2D>(infile,histname),nevpb);
  outfile->cd();
  h1_adaptive.SetName(histname_new.data());
  h1_adaptive.Write();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) h1_adaptive.Print();
  outfile->Close();//avoid double free corruption
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}
