# Documentation for make_TH2A_simple
This script is capable of making a 2D adaptive binning from a single input histogram using command line options only. <br>
The adaptive binning algorithm is documented [here](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning) with code docu [here](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/annotated.html).<br>

## Arguments for the executable
- `-d` : work directory
- `-i` : input file name
- `-o` : ouput file name
- `-t` : **histogram** name (not tree!)
- `-v` : verbosity (1-3, optional)
- nonoptions: minumum number of events (default is 100), name of output histogram (default is old name + "_adaptive"). Note the order of the options. If you only want to change the name, please use `100 "my_new_name"` as option
