#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;//looks like this is not working on the CI runner

#pragma link C++ class TH2A;
#pragma link C++ class AdaptiveBinning<TH2D>;
#pragma link C++ class AdaptiveBinning<TH2F>;
#pragma link C++ class AdaptiveBinning<TH2I>;

#pragma link C++ defined_in "TH2A.h";
#pragma link C++ defined_in "AdaptiveBinning.h";
#endif /* __CINT__ */
