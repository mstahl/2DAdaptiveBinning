/**
  * @file ab01.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2016-08-23
  * @brief This tutorial shows how to construct a TH2A object from an existing binning (can be created by TH2A::WriteBinning())
  */

#include "TROOT.h"
#include "TCanvas.h"
#include "TH2D.h"

#include <utility>

#include "lhcbStyle.h"
#include "TH2A.h"


int main(int argc, char** argv){

  lhcbStyle();
  TH2A::SetVerbosity(3);
  //call ctor generating a TH2A from file (TH2A will check if file exits)
  TH2A myadahisto("binnings/binning_01");

  //set dummy bin-contents
  for(decltype(myadahisto.GetNBins()) i = 1; i <= myadahisto.GetNBins(); i++ )
    myadahisto.SetBinContent(i,i+0.0);

  //plot the adaptive histo
  TCanvas c1("c1","Canvas for adaptive binning",10,10,2048,1280) ;
  gPad->SetTopMargin(0.03);
  gPad->SetBottomMargin(0.15);
  gPad->SetRightMargin(0.13);
  gPad->SetLeftMargin(0.05);

  myadahisto.SetXaxisTitle("X");
  myadahisto.Draw();
  if(!gSystem->OpenDirectory("plots"))gSystem->mkdir("plots");
  c1.SaveAs("plots/ab01_h1.pdf");

  return 0;
}
