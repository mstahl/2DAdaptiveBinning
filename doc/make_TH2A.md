# Documentation for make_2D_adaptive_binning
This script allows for creating 2D adaptive binnings using config files based on [the info parser of boost property trees](http://www.boost.org/doc/libs/1_63_0/doc/html/property_tree/parsers.html#property_tree.parsers.info_parser). <br>
The adaptive binning algorithm is documented [here](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning) with code docu [here](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/annotated.html).<br>
It works with (several) fine-binned histograms as input and will choose the binning accordingly.

## Arguments for the executable
- `-c` : config file
- `-d` : work directory
- `-o` : ouput file name
- `-v` : verbosity (1-3, optional)

## Example config file
coming soon

# Writing a config file
Mandatory child:
- *tasks* : Defines tasks to make binnings which are saved to the output file
    - *NEventsPerBin*   : (double, optional) minimal number of events in one bin (across all input histograms). [More details](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/classTH2A.html#a3f341e9f121ac5febc4b2aa26b0a280f)
    - *SaveBinning*     : (string, optional) the binning will be saved in ASCII format to the given string using [this](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/classTH2A.html#ae1d2a2a989c46d67b59978e5ec6617d5) function
    - *SaveAllHists*    : (bool, optional) fills and saves TH2As from all input histograms. This is not needed when you're only interested in the binning. Be aware that bools are not really bools. So if there is `SaveAllHists 0` in the config file, it will still be parsed as `true`. Just leave it out for false
    - *histograms*      : ***mandatory*** child containing input histograms for this task. The name of the individual node is equal to name of the histogram in the file (unless you give histname below).
        - *filename*    : ***mandatory*** name of the root file containing the histogram (will be searched for locally and within the working directory)
        - *histname*    : (optional, default is the node name) name of the histogram
        - *NewHistName* : (optional, default histname + "_adaptive") name of TH2A in the output file
