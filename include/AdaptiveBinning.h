#ifndef ADAPTIVEBINNING_H
#define ADAPTIVEBINNING_H
//local
#include "TH2A.h"
/**
  * @class AdaptiveBinning
  * @version 0.3
  * @author Marian Stahl
  * @date 2018-02-10
  * @brief Helper class to create a 2D adaptive binning from several input histograms for @c TH2A
  * @details @b Method:@n
  *   Input histograms - @c TH2(D,F,I) - are iteratively splitted at the median of x and y axes.@n
  *   First, the whole histogram is seen as single bin which is split at it's x-median.@n
  *   Then the resulting bins are split at their respective y-median and so forth.
  *   @warning make sure that the input histograms are finely binned, so that the integrals which are calculated are precise enough
  *   @note
  *   - This class is called from TH2A::MakeBinning
  *   - Histograms are used as input since matrices would have to be sorted again in each splitting step. It just seems computationally too expensive.
  *   - The binning has to be copied to TH2A. This could be structured in a better way. Maybe with nested classes
  */
template <class TH2X>
class AdaptiveBinning {
  friend class TH2A;

public:

private:
  enum class              Direction{X,Y};
  const std::vector<TH2X> m_Histos;
  const double            m_MinNumberOfEventsPerBin;
  const TH2X*             m_SmallestHisto;
  unsigned int            m_NHistos = 0u;
  static MessageService   m_ABmsg;
  //hack: define binning here as well and move it later
  std::vector<TH2A::ABin> m_binning;

  /**
   * @fn AdaptiveBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin)
   * @brief Constructor that is called from TH2A::MakeBinning
   */
  AdaptiveBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin)
    : m_Histos{inputs}, m_MinNumberOfEventsPerBin{MinNumberOfEventsPerBin}{
    m_ABmsg.debugmsg("called AdaptiveBinning constructor");
    do_binning();
  }

  /**
   * @fn get_binning()
   * @brief Function to copy the adaptive binning from this class to TH2A
   */
  std::vector<TH2A::ABin> get_binning() const {return m_binning;}
  /**
   * @fn SetVerbosity(int lvl)
   * @param lvl: 0 is ERROR, 1 is WARNING, 2 is INFO, 3 is DEBUG
   * @brief Sets the stdout verbosity
   */
  static void SetVerbosity(MSG_LVL lvl){m_ABmsg.SetMsgLvl(lvl);}

  /**
   * @fn do_binning()
   * @brief Starts splitting chain
   */
  void do_binning();

  /**
   * @fn get_least_entries_in_range(const int binxlo, const int binxhi, const int binylo, const int binyhi)
   * @param binxlo: low bin of the xrange to split (bin of the fine binned input histogram)
   * @param binxhi: high bin of the xrange to split (bin of the fine binned input histogram)
   * @param binylo: low bin of the yrange to split (bin of the fine binned input histogram)
   * @param binyhi: high bin of the yrange to split (bin of the fine binned input histogram)
   * @brief If more than one histogram is given as input, the function sets @c m_SmallestHisto
   * to the histogram with lowest number of entries in the range given by
   * <tt> [binxlo,binxhi]x[binylo,binyhi] </tt> . @c m_IntegralInRange is set to to the number of events in this range
   */
  void get_least_entries_in_range(const int binxlo, const int binxhi, const int binylo, const int binyhi);

  /**
   * @fn split(const int binxlo, const int binxhi, const int binylo, const int binyhi, Direction dir, bool keep_direction)
   * @param binxlo:        low bin of the xrange to split (bin of the fine binned input histogram)
   * @param binxhi:        high bin of the xrange to split (bin of the fine binned input histogram)
   * @param binylo:        low bin of the yrange to split (bin of the fine binned input histogram)
   * @param binyhi:        high bin of the yrange to split (bin of the fine binned input histogram)
   * @param dir:           direction in which to split in
   * @param final_attempt: flags if this is an attempt to split a terminal bin candidate in the other direction
   * @brief This function does all the splitting work and decides wheter to split an adaptive bin
   * further or wheter to save the bin edges and set the bincontent based on @c m_MinNumberOfEventsPerBin
   * @details The bin where the (sub-)histogram will be splitted is determined from @c m_SmallestHisto. @n
   * In case where the adaptive binning is constructed from more than one histogram, this might not always be optimal.@n
   * Especially if the (sub-)histograms have a similar number of entries, but a different shape.@n
   * In such cases, the bin where the (sub-)histograms will be divided can still move,
   * such that new bins of each input histogram has more entries than the required @c m_MinNumberOfEventsPerBin.@n
   * If this method doesn't succed the (sub-)histogram will not be split further and pushed back into @c m_binning
   */
  void split(const int binxlo, const int binxhi, const int binylo, const int binyhi, Direction dir, bool final_attempt=false);

  /**
   * @fn fill_new_bin(const double bc_below, const double bc_above, const int binxlo, const int binxhi, const int binylo, const int binyhi)
   * @param bc_below: bin content below proposed split
   * @param bc_above: bin content above proposed split
   * @param binxlo: low bin of the xrange to split (bin of the fine binned input histogram)
   * @param binxhi: high bin of the xrange to split (bin of the fine binned input histogram)
   * @param binylo: low bin of the yrange to split (bin of the fine binned input histogram)
   * @param binyhi: high bin of the yrange to split (bin of the fine binned input histogram)
   * @brief stores bin-edges and fills an adaptive bin with <tt> bc_below + bc_above </tt> and the corresponding error
   */
  inline void fill_new_bin(const double bc_below, const double bc_above, const int binxlo, const int binxhi, const int binylo, const int binyhi){
    double cont = bc_below + bc_above;
    m_binning.emplace_back(TH2A::ABin{m_SmallestHisto->GetXaxis()->GetBinLowEdge(binxlo),m_SmallestHisto->GetXaxis()->GetBinUpEdge(binxhi),
                           m_SmallestHisto->GetYaxis()->GetBinLowEdge(binylo),m_SmallestHisto->GetYaxis()->GetBinUpEdge(binyhi),
                           cont,std::sqrt(cont),std::sqrt(cont)});
  }

  /**
   * @fn fill_new_bin(const double bc, const int binxlo, const int binxhi, const int binylo, const int binyhi)
   * @param bc: bin content within <tt> [binxlo,binxhi]x[binylo,binyhi] </tt>
   * @param binxlo: low bin of the xrange to split (bin of the fine binned input histogram)
   * @param binxhi: high bin of the xrange to split (bin of the fine binned input histogram)
   * @param binylo: low bin of the yrange to split (bin of the fine binned input histogram)
   * @param binyhi: high bin of the yrange to split (bin of the fine binned input histogram)
   * @brief stores bin-edges and fills an adaptive bin
   */
  inline void fill_new_bin(const double bc, const int binxlo, const int binxhi, const int binylo, const int binyhi){
    m_binning.emplace_back(TH2A::ABin{m_SmallestHisto->GetXaxis()->GetBinLowEdge(binxlo),m_SmallestHisto->GetXaxis()->GetBinUpEdge(binxhi),
                           m_SmallestHisto->GetYaxis()->GetBinLowEdge(binylo),m_SmallestHisto->GetYaxis()->GetBinUpEdge(binyhi),
                           bc,std::sqrt(bc),std::sqrt(bc)});
  }

};
#endif
