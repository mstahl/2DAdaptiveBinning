/**
  * @file TH2A.cpp
  */

#include "TGraph.h"
#include "TDatabasePDG.h"
#include "TParticlePDG.h"

#include "AdaptiveBinning.h"
#include "DalitzHelpers.h"

//set static defaults
TString                  TH2A::m_XAxisTitle = "";
TString                  TH2A::m_YAxisTitle = "";
MessageService           TH2A::m_msgsvc = MessageService("TH2A");
TEfficiency::EStatOption TH2A::m_StatisticsOption = TEfficiency::kFWilson;
double                   TH2A::m_ConfidenceLevel = 0.6827;
double                   TH2A::m_alpha = 0.5;
double                   TH2A::m_beta = 0.5;
short                    TH2A::m_LineWidth = 1;
Color_t                  TH2A::m_LineColor = kBlack;

//public functions

template <typename TH2X> void TH2A::MakeBinning(const std::vector<TH2X>& inputs, const double& MinNumberOfEventsPerBin){
  for(const auto h : inputs)
    if(h.GetEffectiveEntries() <= 2*MinNumberOfEventsPerBin)
      throw std::runtime_error("Not sufficient (effective) entries in the input histogram(s)");
  set_axis_ranges(inputs[0]);
  AdaptiveBinning<TH2X>::SetVerbosity(m_msgsvc.GetMsgLvl());
  AdaptiveBinning<TH2X> adb(inputs,MinNumberOfEventsPerBin);
  m_Binning = adb.get_binning();
  m_XAxisTitle = inputs[0].GetXaxis()->GetTitle();
  m_YAxisTitle = inputs[0].GetYaxis()->GetTitle();
  m_msgsvc.infomsg(TString::Format("Successfully generated 2D adaptive binning with %lu bins",m_Binning.size()-2));
}
template void TH2A::MakeBinning(const std::vector<TH2D>& inputs, const double& MinNumberOfEventsPerBin);
template void TH2A::MakeBinning(const std::vector<TH2F>& inputs, const double& MinNumberOfEventsPerBin);
template void TH2A::MakeBinning(const std::vector<TH2I>& inputs, const double& MinNumberOfEventsPerBin);

template <typename TH2X> void TH2A::Weight(const TH2X& origin, const TH2X& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor){
  set_axis_ranges(origin);
  AdaptiveBinning<TH2X>::SetVerbosity(m_msgsvc.GetMsgLvl());
  AdaptiveBinning<TH2X> adb({origin,target},MinNumberOfEventsPerBin);
  m_Binning = adb.get_binning();
  m_XAxisTitle = origin.GetXaxis()->GetTitle();
  m_YAxisTitle = origin.GetYaxis()->GetTitle();
  m_msgsvc.infomsg(TString::Format("Successfully generated 2D adaptive binning with %lu bins",m_Binning.size()-2));
  TH2A origin_with_binning(*this);
  TH2A target_with_binning(*this);
  origin_with_binning.FillFromHist(origin);
  target_with_binning.FillFromHist(target);
  for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++){
    if(abs(origin_with_binning.GetBinContent(ibin)) > 0.){
      m_Binning[ibin].content =  target_with_binning.GetBinContent(ibin)/origin_with_binning.GetBinContent(ibin);
      m_Binning[ibin].error_lo = m_Binning[ibin].content * std::sqrt(std::pow(target_with_binning.GetBinErrorLow(ibin)/target_with_binning.GetBinContent(ibin),2)
                                                                   + std::pow(origin_with_binning.GetBinErrorLow(ibin)/origin_with_binning.GetBinContent(ibin),2));
      m_Binning[ibin].error_hi = m_Binning[ibin].content * std::sqrt(std::pow(target_with_binning.GetBinErrorUp(ibin)/target_with_binning.GetBinContent(ibin),2)
                                                                   + std::pow(origin_with_binning.GetBinErrorUp(ibin)/origin_with_binning.GetBinContent(ibin),2));
    }
    else {
      m_Binning[ibin].content = 0.;
      m_Binning[ibin].error_lo = 0.;
      m_Binning[ibin].error_hi = 0.;
    }
  }
  // truncate ; get truncation limit from weighted mean first and set outliers to 0
  if(truncation_factor>0){
    const auto truncation_limit = truncation_factor*this->GetSumOfWeights()/this->GetNBins(); // take simple average
    m_msgsvc.debugmsg("Weight truncation limit "+std::to_string(truncation_limit));
    for(unsigned int ibin = 0; ibin < GetNBins()+2; ibin++)
      if(m_Binning[ibin].content > truncation_limit) {
        TString bin_info;
        if(m_Binning[ibin].error_lo == m_Binning[ibin].error_hi)
          bin_info = TString::Format("Bin %-4u: %-8.4g  %-8.4g  %-8.4g  %-8.4g  Content: %-8.4g +- %-8.4g",
                                      ibin, m_Binning[ibin].xlo,m_Binning[ibin].xhi,m_Binning[ibin].ylo,m_Binning[ibin].yhi,m_Binning[ibin].content,m_Binning[ibin].error_hi);
        else
          bin_info = TString::Format("Bin %-4u: %-8.4g  %-8.4g  %-8.4g  %-8.4g  Content: %-8.4g - %-8.4g + %-8.4g",
                                      ibin,m_Binning[ibin].xlo,m_Binning[ibin].xhi,m_Binning[ibin].ylo,m_Binning[ibin].yhi,m_Binning[ibin].content,m_Binning[ibin].error_lo,m_Binning[ibin].error_hi);
        m_msgsvc.warningmsg("Weight truncation for "+bin_info);
        m_Binning[ibin].content = 0;
        m_Binning[ibin].error_lo = 0;
        m_Binning[ibin].error_hi = 0;
      }
  }
}
template void TH2A::Weight(const TH2D& origin, const TH2D& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor);
template void TH2A::Weight(const TH2F& origin, const TH2F& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor);
template void TH2A::Weight(const TH2I& origin, const TH2I& target, const double& MinNumberOfEventsPerBin, const double& truncation_factor);

void TH2A::Draw(Option_t* option){

  if(!(static_cast<TString>(option) == "" || static_cast<TString>(option) == "colz" || static_cast<TString>(option) == "COLZ"))
    m_msgsvc.warningmsg(TString::Format("Option %s is not implemented",option));

  TCanvas* c = static_cast<TCanvas*>(gROOT->GetListOfCanvases()->At(0));
  if(c == nullptr)c = TCanvas::MakeDefCanvas();

  m_dummy = TH2D("dummy","",m_NDummyBins,m_XLo,m_XHi,m_NDummyBins,m_YLo,m_YHi);
  m_dummy.GetXaxis()->SetTitle(m_XAxisTitle);
  m_dummy.GetYaxis()->SetTitle(m_YAxisTitle);

  if(m_ZHi == -999.9)
    m_ZHi = (*std::max_element(m_Binning.begin()+1,m_Binning.end()-1,[](ABin a, ABin b){return a.content < b.content;})).content;
  m_dummy.SetMaximum(m_ZHi);

  for(decltype(m_dummy.GetNbinsX()) xbiniter = 1; xbiniter < m_dummy.GetNbinsX(); xbiniter++ ){
    for(decltype(m_dummy.GetNbinsY()) ybiniter = 1; ybiniter < m_dummy.GetNbinsY(); ybiniter++ ){
      m_dummy.SetBinContent(xbiniter,ybiniter,m_ZHi);
    }
  }

  if(m_ZLo == 999.9)
    m_ZLo = (*std::min_element(m_Binning.begin()+1,m_Binning.end()-1,[](ABin a, ABin b){return a.content < b.content;})).content;

  //just set a random bin to the lowest element (not at the edge though)
  m_dummy.SetBinContent(2,2,m_ZLo);
  m_dummy.SetMinimum(m_ZLo);
  m_msgsvc.debugmsg(TString::Format("Set ZRange to [%.6g,%.6g]",m_ZLo,m_ZHi));
  m_dummy.Draw("colz");//to get the palette we want
  c->Update();

  //Get the right colors and plot the beast
  const int ndivz = gStyle->GetNumberContours();
  const int ncolors  = gStyle->GetNumberOfColors();
  const double zscale = ndivz/(m_ZHi-m_ZLo);

  for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
    int color = int(0.01+(m_Binning[ibin].content-m_ZLo)*zscale);
    int theColor = int((color+0.99)*double(ncolors)/double(ndivz));
    if (theColor > ncolors-1) theColor = ncolors-1;
    Color_t boxcolor = (Color_t)gStyle->GetColorPalette(theColor);

    //avoid drawing boxes outside the range
    auto xlo_to_draw = m_Binning[ibin].xlo, ylo_to_draw = m_Binning[ibin].ylo, xhi_to_draw = m_Binning[ibin].xhi, yhi_to_draw = m_Binning[ibin].yhi;
    if(xlo_to_draw < m_XLo)xlo_to_draw = m_XLo;
    if(xlo_to_draw > m_XHi)xlo_to_draw = m_XHi;
    if(xhi_to_draw < m_XLo)xhi_to_draw = m_XLo;
    if(xhi_to_draw > m_XHi)xhi_to_draw = m_XHi;
    if(ylo_to_draw < m_YLo)ylo_to_draw = m_YLo;
    if(ylo_to_draw > m_YHi)ylo_to_draw = m_YHi;
    if(yhi_to_draw < m_YLo)yhi_to_draw = m_YLo;
    if(yhi_to_draw > m_YHi)yhi_to_draw = m_YHi;

    TBox box(xlo_to_draw,ylo_to_draw,xhi_to_draw,yhi_to_draw);
    box.SetFillColor(boxcolor);
    box.SetLineColor(boxcolor);
    box.DrawClone("same");
    TLine lline(xlo_to_draw,ylo_to_draw,xlo_to_draw,yhi_to_draw);
    lline.SetLineWidth(m_LineWidth);
    lline.SetLineColor(m_LineColor);
    lline.DrawClone("same");
    TLine bline(xlo_to_draw,ylo_to_draw,xhi_to_draw,ylo_to_draw);
    bline.SetLineWidth(m_LineWidth);
    bline.SetLineColor(m_LineColor);
    bline.DrawClone("same");
  }

  c->Update();

  return;
}

void TH2A::DrawDPBoundary(const char* mother, const char* dau1, const char* dau2, const char* dau3, const int& nbins) const {
  m_msgsvc.infomsg(TString::Format("Requested to draw Dalitz plot boundaries of %s -> %s %s %s",mother,dau1,dau2,dau3));
  TDatabasePDG pdgbd;
  auto mother_pdg = pdgbd.GetParticle(mother);
  auto dau1_pdg   = pdgbd.GetParticle(dau1);
  auto dau2_pdg   = pdgbd.GetParticle(dau2);
  auto dau3_pdg   = pdgbd.GetParticle(dau3);

  if(mother_pdg == nullptr || dau1_pdg == nullptr || dau2_pdg == nullptr || dau3_pdg == nullptr){
    m_msgsvc.warningmsg("At least one of the particles is not known to TDatabasePDG. Can't draw Dalitz boundary.");
    return;
  }
  DrawDPBoundary(1000*mother_pdg->Mass(),1000*dau1_pdg->Mass(),1000*dau2_pdg->Mass(),1000*dau3_pdg->Mass(),nbins);
}

void TH2A::DrawDPBoundary(std::string mother, std::string dau1, std::string dau2, std::string dau3, const int& nbins) const {

  if(!(dau1.find("_min") != std::string::npos || dau2.find("_min") != std::string::npos || dau3.find("_min") != std::string::npos)){
    m_msgsvc.infomsg(TString::Format("Requested to draw Dalitz plot boundaries of %s -> %s %s %s",mother.data(),dau1.data(),dau2.data(),dau3.data()));
    TDatabasePDG pdgbd;
    auto mother_pdg = pdgbd.GetParticle(mother.data());
    auto dau1_pdg   = pdgbd.GetParticle(dau1.data());
    auto dau2_pdg   = pdgbd.GetParticle(dau2.data());
    auto dau3_pdg   = pdgbd.GetParticle(dau3.data());

    if(mother_pdg == nullptr || dau1_pdg == nullptr || dau2_pdg == nullptr || dau3_pdg == nullptr){
      m_msgsvc.warningmsg("At least one of the particles is not known to TDatabasePDG. Can't draw Dalitz boundary.");
      return;
    }
    DrawDPBoundary(mother_pdg->Mass(),dau1_pdg->Mass(),dau2_pdg->Mass(),dau3_pdg->Mass(),nbins);
  }
  else {//we're doomed
    int topology = -1; std::string sym_pn, other_pn;
    if(dau1.find("_min") != std::string::npos && dau2.find("_max") != std::string::npos){
      topology = 5; dau1.erase(dau1.end()-4,dau1.end()); sym_pn = dau1; other_pn = dau3;
    }
    else if(dau1.find("_max") != std::string::npos && dau2.find("_min") != std::string::npos){
      topology = 4; dau1.erase(dau1.end()-4,dau1.end()); sym_pn = dau1; other_pn = dau3;
    }
    else if(dau1.find("_min") != std::string::npos && dau3.find("_max") != std::string::npos){
      topology = 0; dau1.erase(dau1.end()-4,dau1.end()); sym_pn = dau1; other_pn = dau2;
    }
    else if(dau1.find("_max") != std::string::npos && dau3.find("_min") != std::string::npos){
      topology = 2; dau1.erase(dau1.end()-4,dau1.end()); sym_pn = dau1; other_pn = dau2;
    }
    else if(dau2.find("_min") != std::string::npos && dau3.find("_max") != std::string::npos){
      topology = 1; dau2.erase(dau2.end()-4,dau2.end()); sym_pn = dau2; other_pn = dau1;
    }
    else if(dau2.find("_max") != std::string::npos && dau3.find("_min") != std::string::npos){
      topology = 3; dau2.erase(dau2.end()-4,dau2.end()); sym_pn = dau2; other_pn = dau1;
    }
    else m_msgsvc.warningmsg("Trying to parse the DP topology, but can't figure it out...");
    m_msgsvc.infomsg(TString::Format("Requested to draw symmetric Dalitz plot boundaries of %s -> %s %s %s, topology %d",
                                     mother.data(),other_pn.data(),sym_pn.data(),sym_pn.data(),topology));
    TDatabasePDG pdgbd;
    auto mother_pdg = pdgbd.GetParticle(mother.data());
    auto other_pdg  = pdgbd.GetParticle(other_pn.data());
    auto sym_pdg    = pdgbd.GetParticle(sym_pn.data());
    if(mother_pdg == nullptr || other_pdg == nullptr || sym_pdg == nullptr){
      m_msgsvc.warningmsg("At least one of the particles is not known to TDatabasePDG. Can't draw symmetric Dalitz plot boundary.");
      return;
    }
    DrawSymmetricDPBoundary(mother_pdg->Mass(),sym_pdg->Mass(),other_pdg->Mass(),topology,nbins);
  }
}

template <typename F>
void TH2A::DrawDPBoundary(const F& m_mother, const F& m_dau1, const F& m_dau2, const F& m_dau3, const int& nbins,
                          const F& offsetX, const F& offsetY) const {

  auto m12SqMin = std::pow((m_dau1 + m_dau2),2);
  auto m12SqMax = std::pow((m_mother - m_dau3),2);

  //functions defined in DalitzHelpers.h. Calculations rely on http://pdg.lbl.gov/2019/reviews/rpp2018-rev-kinematics.pdf 47.4.3.1. Dalitz plot
  TF1 dpp("dpp",&upper_DP_boundary,m12SqMin,m12SqMax,4);
  TF1 dpm("dpm",&lower_DP_boundary,m12SqMin,m12SqMax,4);

  auto set_TF1_stuff = [&nbins,&m_mother,&m_dau1,&m_dau2,&m_dau3] (TF1& myf) {
    myf.SetParameter(0,m_mother);
    myf.SetParameter(1,m_dau1);
    myf.SetParameter(2,m_dau2);
    myf.SetParameter(3,m_dau3);
    myf.SetNpx(nbins);
  };

  set_TF1_stuff(dpm);
  set_TF1_stuff(dpp);

  TF1 minf("minf",TString::Format("%.0f",static_cast<double>(std::floor(m_YLo-offsetY-1))).Data(),m12SqMin,m12SqMax);
  minf.SetNpx(nbins);
  TF1 maxf("maxf",TString::Format("%.0f",static_cast<double>(std::ceil(m_YHi-offsetY+1))).Data(),m12SqMin,m12SqMax);
  maxf.SetNpx(nbins);

  //shade defined in DalitzHelpers.h
  auto gr1 = shade(minf,dpm,m_XLo-1,m_XHi+1,m_YLo-1,m_YHi+1,offsetX,offsetY);
  auto gr2 = shade(dpp,maxf,m_XLo-1,m_XHi+1,m_YLo-1,m_YHi+1,offsetX,offsetY);

  if(m_dummy.GetXaxis()->GetXmin() != m_XLo || m_dummy.GetYaxis()->GetXmin() != m_YLo ||
     m_dummy.GetXaxis()->GetXmax() != m_XHi || m_dummy.GetYaxis()->GetXmax() != m_YHi)
    m_msgsvc.warningmsg("You should call TH2A::Draw() before TH2A::DrawDPBoundary()");

  //give these beasts a name so they can be manipulated afterwards: static_cast<TGraph*>(gROOT->Get("dpblo"));
  gr1.SetName("dpblo");
  gr2.SetName("dpbhi");

  auto set_draw_stuff = [] (TGraph& mygraph) {
    mygraph.SetFillColor(kGray);
    mygraph.SetFillStyle(1001);
    mygraph.SetMarkerColor(kBlack);
    mygraph.SetMarkerSize(2);
  };

  set_draw_stuff(gr1);
  set_draw_stuff(gr2);
  gr1.DrawClone("fsame");
  gr2.DrawClone("pfsame");
  gr1.DrawClone("psame");
  m_dummy.Draw("axissame");
}
template void TH2A::DrawDPBoundary(const double& m_mother, const double& m_dau1, const double& m_dau2, const double& m_dau3, const int& nbins,
const double& offsetX, const double& offsetY) const;
template void TH2A::DrawDPBoundary(const float& m_mother, const float& m_dau1, const float& m_dau2, const float& m_dau3, const int& nbins,
const float& offsetX, const float& offsetY) const;
template void TH2A::DrawDPBoundary(const long double& m_mother, const long double& m_dau1, const long double& m_dau2, const long double& m_dau3, const int& nbins,
const long double& offsetX, const long double& offsetY) const;

void TH2A::DrawSymmetricDPBoundary(double&& M, double&& m_sym, double&& m_other, const int& topology, const int& nbins ) const {
  //with m12 as x axis, m23 as axis the following configurations (cases) exist:
  //0 : m12 = (m_sym,m_other)_min, m23 = (m_other,m_sym)_max, m13 = (m_sym,m_sym)       ==> 1,3 are "symmetric", m12 <= m23
  //1 : m12 = (m_other,m_sym)_min, m23 = (m_sym,m_sym),       m13 = (m_other,m_sym)_max ==> 2,3 are "symmetric", m12 <= m23
  //2 : m12 = (m_sym,m_other)_max, m23 = (m_other,m_sym)_min, m13 = (m_sym,m_sym)       ==> 1,3 are "symmetric", m12 >= m23
  //3 : m12 = (m_other,m_sym)_max, m23 = (m_sym,m_sym),       m13 = (m_other,m_sym)_min ==> 2,3 are "symmetric", m12 >= m23
  //4 : m12 = (m_sym,m_sym),       m23 = (m_sym,m_other)_min, m13 = (m_sym,m_other)_max ==> 1,2 are "symmetric", m23 <= m13
  //5 : m12 = (m_sym,m_sym),       m23 = (m_sym,m_sym)_max,   m13 = (m_sym,m_other)_min ==> 1,2 are "symmetric", m23 >= m13
  // "symmetric" means that both particles are of the exact same type, like pi+ pi+

  //endpoints
  auto min_ftr = [] (auto&& m1, auto&& m2) {return std::pow((m1+m2),2);};
  auto max_ftr = [&M] (auto&& m) {return std::pow((M-m),2);};
  //endpoints of "sym+sym" combination in "other+sym" system
  auto mos_at_mssmin_ftr = [&M] (auto&& ms, auto&& mo) {return pow(CalcGizmo::pr_endpoint(M,M-mo,ms,ms,mo),2);};
  auto mos_at_mssmax_ftr = [&M] (auto&& ms, auto&& mo) {return pow(CalcGizmo::pr_endpoint(M,2*ms,ms,ms,mo),2);};
  TF1 lbsDP; //function for lower boundary
  TF1 ubsDP; //function for upper boundary
  switch (topology) {
    case 0: lbsDP = TF1("l",&lower_boundary_so_so,min_ftr(m_sym,m_other),mos_at_mssmax_ftr(m_sym,m_other),5);
            lbsDP.SetParameters(M,m_sym,m_other,m_sym,mos_at_mssmin_ftr(m_sym,m_other));
            ubsDP = TF1("u",&upper_DP_boundary,min_ftr(m_sym,m_other),mos_at_mssmax_ftr(m_sym,m_other),4);
            ubsDP.SetParameters(M,m_sym,m_other,m_sym);
            break;
    case 1: lbsDP = TF1("l",&lower_DP_boundary,min_ftr(m_sym,m_other),mos_at_mssmax_ftr(m_sym,m_other),4);
            lbsDP.SetParameters(M,m_other,m_sym,m_sym);
            ubsDP = TF1("u",&upper_boundary_so_ss,min_ftr(m_sym,m_other),mos_at_mssmax_ftr(m_sym,m_other),5);
            ubsDP.SetParameters(M,m_other,m_sym,m_sym,mos_at_mssmin_ftr(m_sym,m_other));
            break;
    case 2: lbsDP = TF1("l",&lower_DP_boundary,mos_at_mssmin_ftr(m_sym,m_other),max_ftr(m_sym),4);
            lbsDP.SetParameters(M,m_sym,m_other,m_sym);
            ubsDP = TF1("u",&upper_boundary_so_so,mos_at_mssmin_ftr(m_sym,m_other),max_ftr(m_sym),5);
            ubsDP.SetParameters(M,m_sym,m_other,m_sym,mos_at_mssmax_ftr(m_sym,m_other));
            break;
    case 3: lbsDP = TF1("l",&lower_boundary_so_ss,mos_at_mssmin_ftr(m_sym,m_other),max_ftr(m_sym),5);
            lbsDP.SetParameters(M,m_other,m_sym,m_sym,mos_at_mssmax_ftr(m_sym,m_other));
            ubsDP = TF1("u",&upper_DP_boundary,mos_at_mssmin_ftr(m_sym,m_other),max_ftr(m_sym),4);
            ubsDP.SetParameters(M,m_other,m_sym,m_sym);
            break;
    case 4: lbsDP = TF1("l",&lower_DP_boundary,min_ftr(m_sym,m_sym),max_ftr(m_other),4);
            lbsDP.SetParameters(M,m_sym,m_sym,m_other);
            ubsDP = TF1("u",&cut_through_the_DP,min_ftr(m_sym,m_sym),max_ftr(m_other),4);
            ubsDP.SetParameters(mos_at_mssmax_ftr(m_sym,m_other),mos_at_mssmin_ftr(m_sym,m_other),min_ftr(m_sym,m_sym),max_ftr(m_other));
            break;
    case 5: lbsDP = TF1("l",&cut_through_the_DP,min_ftr(m_sym,m_sym),max_ftr(m_other),4);
            lbsDP.SetParameters(mos_at_mssmax_ftr(m_sym,m_other),mos_at_mssmin_ftr(m_sym,m_other),min_ftr(m_sym,m_sym),max_ftr(m_other));
            ubsDP = TF1("u",&upper_DP_boundary,min_ftr(m_sym,m_sym),max_ftr(m_other),4);
            ubsDP.SetParameters(M,m_sym,m_sym,m_other);
            break;
    default:
      m_msgsvc.warningmsg(TString::Format("Unkown topology: %d. Valid: 0 to 5",topology));
  }
  lbsDP.SetNpx(nbins);
  ubsDP.SetNpx(nbins);

  TF1 minf("minf",TString::Format("%.0f",static_cast<double>(std::floor(m_YLo-1))).Data(),lbsDP.GetXmin(),lbsDP.GetXmax());
  minf.SetNpx(nbins);
  TF1 maxf("maxf",TString::Format("%.0f",static_cast<double>(std::ceil(m_YHi+1))).Data(),lbsDP.GetXmin(),lbsDP.GetXmax());
  maxf.SetNpx(nbins);

  //shade defined in DalitzHelpers.h
  auto gr1 = shade(minf,lbsDP,m_XLo-1,m_XHi+1,m_YLo-1,m_YHi+1,0.,0.);
  auto gr2 = shade(ubsDP,maxf,m_XLo-1,m_XHi+1,m_YLo-1,m_YHi+1,0.,0.);

  if(m_dummy.GetXaxis()->GetXmin() != m_XLo || m_dummy.GetYaxis()->GetXmin() != m_YLo ||
     m_dummy.GetXaxis()->GetXmax() != m_XHi || m_dummy.GetYaxis()->GetXmax() != m_YHi)
    m_msgsvc.warningmsg("You should call TH2A::Draw() before TH2A::DrawDPBoundary()");

  //give these beasts a name so they can be manipulated afterwards: static_cast<TGraph*>(gROOT->Get("dpblo"));
  gr1.SetName("dpblo");
  gr2.SetName("dpbhi");

  auto set_draw_stuff = [] (TGraph& mygraph) {
    mygraph.SetFillColor(kGray);
    mygraph.SetFillStyle(1001);
    mygraph.SetMarkerColor(kBlack);
    mygraph.SetMarkerSize(2);
  };

  set_draw_stuff(gr1);
  set_draw_stuff(gr2);
  gr1.DrawClone("fsame");
  gr2.DrawClone("pfsame");
  gr1.DrawClone("psame");
  m_dummy.Draw("axissame");
}

bool TH2A::HasSameBinning(const TH2A& other_hist) const {
  if(this == &other_hist){
    m_msgsvc.debugmsg("Pointers of input TH2As match");
    return true;
  }
  if(m_Binning.size() == 0u) return false;
  for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
    if(this->GetBinLowEdgeX(ibin) != other_hist.GetBinLowEdgeX(ibin) || this->GetBinLowEdgeY(ibin) != other_hist.GetBinLowEdgeY(ibin) ||
       this->GetBinUpEdgeX(ibin) != other_hist.GetBinUpEdgeX(ibin) || this->GetBinUpEdgeY(ibin) != other_hist.GetBinUpEdgeY(ibin))
      return false;
  }
  return true;
}

int TH2A::BinomialDivide(const TH2A& numerator, const TH2A& denominator, const bool fix_eff){

  if(!numerator.HasSameBinning(denominator))throw std::runtime_error("BinomialDivide: binning of input histograms do not match");
  //in case the calling TH2A is empty or has different Binning
  if(!this->HasSameBinning(numerator)){
    *this = numerator;//copy binning from numerator
    m_msgsvc.infomsg("Copied binning from numerator");
  }

  //loop over bins, get efficiency and error, fill TH2A contents
  auto warning_counter = 0u;
  int status = 0;
  for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
    double total = denominator.GetBinContent(ibin);
    double passed = numerator.GetBinContent(ibin);
    double efficiency = passed/total;
    m_Binning[ibin].content = efficiency;

    //set the status to return
    if(0 > efficiency || efficiency > 1){
      if(status <= 2 && 0 > efficiency && numerator.GetBinContent(ibin) + 2*numerator.GetBinErrorUp(ibin) >= 0)
        status = 2;
      else if(status <= 2 && efficiency > 1 && numerator.GetBinContent(ibin) - 2*numerator.GetBinErrorLow(ibin) <= denominator.GetBinContent(ibin))
        status = 2;
      else status = 3;
    }

    //compute the uncertainty
    try {
      double rtot = std::round(total);
      double rpass = std::round(passed);
      //fix for statistical fluctuations
      if(fix_eff && (total <= 0 || passed < 0)){
        m_Binning[ibin].content = 0;
        if(total <= 0){
          rpass = 0;
          rtot = 0;
        }
        if(passed < 0) rpass = 0;
        m_msgsvc.debugmsg(TString::Format("Efficiency less than 0. Fixed efficiency to be 0 and "
                                          "the number of passed events at %.0f and the number of total events at %.0f for the calculation of uncertainties",rpass,rtot));
      }
      if(fix_eff && passed > total){
        m_Binning[ibin].content = 1;
        rpass = rtot;
        m_msgsvc.debugmsg(TString::Format("Efficiency bigger than 1. Fixed efficiency to be 1 and "
                                          "the number of passed events at the number of total events (%.0f) for the calculation of uncertainties",rpass));
      }

      if( total != rtot || passed != rpass )
        m_msgsvc.debugmsg(TString::Format("Rounding total from %.6g to %.0f and passed from %.6g to %.0f to prevent implicit cast to int by TEfficiency method",total,rtot,passed,rpass));
      //throw warning if the number of passed events is bigger than the number of total events
      if(rtot < rpass){
        if(warning_counter < 5)m_msgsvc.warningmsg(TString::Format("BinomialDivide: the number of passed events %.0f is bigger than the number of total events %.0f "
                                                                   "in bin %-4u: x[%-8.4g, %-8.4g] y[%-8.4g, %-8.4g]",
                                                                   rpass,rtot,ibin,m_Binning[ibin].xlo,m_Binning[ibin].xhi,m_Binning[ibin].ylo,m_Binning[ibin].yhi));
        warning_counter++;
      }
      double confidence_interval_lo = this->GetEffCI(rtot,rpass,false);
      double confidence_interval_hi = this->GetEffCI(rtot,rpass,true);
      m_Binning[ibin].error_lo = m_Binning[ibin].content-confidence_interval_lo;
      m_Binning[ibin].error_hi = confidence_interval_hi-m_Binning[ibin].content;
      if(status < 1 && (confidence_interval_lo < 0 || confidence_interval_hi > 1))
        status = 1;
    }
    catch(std::range_error& e){
      m_msgsvc.errormsg(e.what());
      throw;
    }
  }
  if(warning_counter >= 5)
    m_msgsvc.warningmsg(TString::Format("BinomialDivide: Counted %u bins in which the number of passed events is bigger than the number of total events",warning_counter));

  return status;
}

void TH2A::BayesianCombine(const TH2A& numerator1, const TH2A& denominator1, const TH2A& numerator2, const TH2A& denominator2){
  //loop over bins, combine efficiencies

  for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
    double confidence_interval_lo, confidence_interval_hi;
    int pass [2] = {static_cast<int>(std::round(numerator1.GetBinContent(ibin))),static_cast<int>(std::round(numerator2.GetBinContent(ibin)))};
    int total[2] = {static_cast<int>(std::round(denominator1.GetBinContent(ibin))),static_cast<int>(std::round(denominator2.GetBinContent(ibin)))};
    double weights[2] = {1.0,1.0};
    //to get the posterior, we need passed and total of the efficiencies we want to combine
    auto comb_eff = TEfficiency::Combine(confidence_interval_hi,confidence_interval_lo,2,pass,total,m_alpha,m_beta,m_ConfidenceLevel,weights,"");
    m_Binning[ibin].content = comb_eff;
    m_Binning[ibin].error_lo = comb_eff-confidence_interval_lo;
    m_Binning[ibin].error_hi = confidence_interval_hi-comb_eff;
  }
}

void TH2A::BayesianCombine(const std::vector<TH2A>& numerators, const std::vector<TH2A>& denominators){

  auto n = numerators.size();
  if(n != denominators.size() || n < 2)
    throw std::runtime_error("For combination, the number of numerators should be equal to the number of denominators! Also, the size of the vectors should be >= 2");

  //loop over bins, combine efficiencies
  for(unsigned int ibin = 1; ibin <= GetNBins(); ibin++){
    double confidence_interval_lo, confidence_interval_hi;
    int pass [n], total[n];double weights[2] = {1.0,1.0};
    for(decltype(n) i = 0; i < n; i++){
      pass [i] = static_cast<int>(std::round(numerators[i].GetBinContent(ibin)));
      total[i] = static_cast<int>(std::round(denominators[i].GetBinContent(ibin)));
    }

    //to get the posterior, we need passed and total of the efficiencies we want to combine
    auto comb_eff = TEfficiency::Combine(confidence_interval_hi,confidence_interval_lo,n,pass,total,m_alpha,m_beta,m_ConfidenceLevel,weights,"");
    m_Binning[ibin].content = comb_eff;
    m_Binning[ibin].error_lo = comb_eff-confidence_interval_lo;
    m_Binning[ibin].error_hi = confidence_interval_hi-comb_eff;
  }
}

//private functions

double TH2A::GetEffCI(const double& total, const double& passed, const bool& bUpper) const {
  if(m_StatisticsOption == TEfficiency::kFCP){
    return TEfficiency::ClopperPearson(total,passed,m_ConfidenceLevel,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kFNormal){
    return TEfficiency::Normal(total,passed,m_ConfidenceLevel,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kFWilson){
    return TEfficiency::Wilson(total,passed,m_ConfidenceLevel,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kFAC){
    return TEfficiency::AgrestiCoull(total,passed,m_ConfidenceLevel,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kFFC){
    return TEfficiency::FeldmanCousins(total,passed,m_ConfidenceLevel,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kBJeffrey){
    return TEfficiency::Bayesian(total,passed,m_ConfidenceLevel,0.5,0.5,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kBUniform){
    return TEfficiency::Bayesian(total,passed,m_ConfidenceLevel,1,1,bUpper);
  }
  else if(m_StatisticsOption == TEfficiency::kBBayesian){
    return TEfficiency::Bayesian(total,passed,m_ConfidenceLevel,m_alpha,m_beta,bUpper);
  }
  else{
    throw std::range_error("Only valid from 0-7 or in TEfficiency namespace: kFCP, kFNormal, kFWilson, kFAC, kFFC, kBJeffrey, kBUniform, kBBayesian");
  }
}
