/**
  * @file AdaptiveBinning.cpp
  */

#include "AdaptiveBinning.h"

template <class TH2X> MessageService AdaptiveBinning<TH2X>::m_ABmsg = MessageService("TH2A:AdaptiveBinning");

template class AdaptiveBinning<TH2D>;
template class AdaptiveBinning<TH2F>;
template class AdaptiveBinning<TH2I>;

//private functions
template <class TH2X>
void AdaptiveBinning<TH2X>::do_binning(){
  m_NHistos = static_cast<int>(m_Histos.size());
  m_ABmsg.infomsg(TString::Format("Generating 2D adaptive binning with at least %.6g events "
                                  "per bin using %u input histograms. Please be patient... ",
                                  m_MinNumberOfEventsPerBin, m_NHistos));
  //get the histo with least entries which will be split later
  m_SmallestHisto = &(*std::min_element(m_Histos.begin(), m_Histos.end(),[](const TH2X& histA,const TH2X& histB){
    return histA.GetEffectiveEntries() < histB.GetEffectiveEntries();}));
  //underflow
  m_binning.emplace_back( TH2A::ABin{-std::nan(""),-std::nan(""),-std::nan(""),-std::nan(""),0.0,0.0,0.0} );
  //this starts the splitting chain, the rest is done in the functions themselves
  split(1,m_SmallestHisto->GetXaxis()->GetNbins(),1,m_SmallestHisto->GetYaxis()->GetNbins(),Direction::X);
  //overflow
  m_binning.emplace_back( TH2A::ABin{std::nan(""),std::nan(""),std::nan(""),std::nan(""),0.0,0.0,0.0} );
  return;
}

template <class TH2X>
void AdaptiveBinning<TH2X>::get_least_entries_in_range(const int binxlo, const int binxhi, const int binylo, const int binyhi) {
  m_ABmsg.debugmsg(TString::Format("Range X [%.6g,%.6g] Y [%.6g,%.6g]",
                                   m_Histos[0].GetXaxis()->GetBinLowEdge(binxlo),m_Histos[0].GetXaxis()->GetBinLowEdge(binxhi),
      m_Histos[0].GetYaxis()->GetBinLowEdge(binylo),m_Histos[0].GetYaxis()->GetBinLowEdge(binyhi)));
  double* ContentInRange = new double[m_NHistos];
  int* ArrayOfIndices = new int[m_NHistos];
  for(unsigned int iter = 0u; iter < m_NHistos ; iter++) {
    ContentInRange[iter] = m_Histos[iter].Integral(binxlo,binxhi,binylo,binyhi);
    m_ABmsg.debugmsg(TString::Format("content_in_range[%u] = %.6g",iter,ContentInRange[iter]));
    ArrayOfIndices[iter] = iter;
    m_ABmsg.debugmsg(TString::Format("array_of_indices[%u] = %d",iter,ArrayOfIndices[iter]));
  }
  TMath::BubbleLow(m_NHistos,ContentInRange,ArrayOfIndices);
  m_ABmsg.debugmsg(TString::Format("Name of smallest histogram in range: %s",m_Histos[ArrayOfIndices[0]].GetName()));
  m_ABmsg.debugmsg(TString::Format("Number of events of smallest histogram in range %.2f",ContentInRange[ArrayOfIndices[0]]));
  m_SmallestHisto = &m_Histos[ArrayOfIndices[0]];
  delete ContentInRange;
  delete ArrayOfIndices;
  return;
}

template <class TH2X>
void AdaptiveBinning<TH2X>::split(const int binxlo, const int binxhi, const int binylo, const int binyhi, Direction dir, bool final_attempt) {
  final_attempt ? m_ABmsg.debugmsg(TString::Format("attempting a final split along %c",dir == Direction::X ? 'X' : 'Y')) : m_ABmsg.debugmsg(TString::Format("splitting along %c",dir == Direction::X ? 'X' : 'Y'));

  //calculate the median along the "dir"-axis (we're only interested in the binnumber)
  const auto integral_subhist = m_SmallestHisto->Integral(binxlo,binxhi,binylo,binyhi);
  //functor for the projection integral (much faster than doing the projection and calculating the integral)
  std::function<double(int&)> get_projected_BinContent;
  if(dir == Direction::X)
    get_projected_BinContent = [this,&binyhi,&binylo] (int& thebin) -> double {return m_SmallestHisto->Integral(thebin,thebin,binylo,binyhi);};
  else
    get_projected_BinContent = [this,&binxhi,&binxlo] (int& thebin) -> double {return m_SmallestHisto->Integral(binxlo,binxhi,thebin,thebin);};

  //initialise the bin that will be median
  int splitbin = dir == Direction::X ? binxlo : binylo;
  auto sum = integral_subhist - get_projected_BinContent(splitbin);

  while(sum > integral_subhist/2){
    ++splitbin;
    sum -= get_projected_BinContent(splitbin);
  }

  //we found the median, now calculate the integral below and above the median bin
  std::vector<double> bcs_below;
  std::vector<double> bcs_above;
  auto fill_bc_vectors = [this, &dir, &bcs_below, &bcs_above, &binxlo, &binxhi, &binylo, &binyhi, &splitbin] () -> void {
    for (const TH2X& hist : m_Histos){
      if(dir == Direction::X){
        bcs_below.push_back(hist.Integral(binxlo,splitbin,binylo,binyhi));
        bcs_above.push_back(hist.Integral(splitbin+1,binxhi,binylo,binyhi));
      }
      else{
        bcs_below.push_back(hist.Integral(binxlo,binxhi,binylo,splitbin));
        bcs_above.push_back(hist.Integral(binxlo,binxhi,splitbin+1,binyhi));
      }
    }
  };

  //call the lambda (it gets used later again)
  fill_bc_vectors();

  // We don't want to split if there are less entries in the splitted bins than m_MinNumberOfEventsPerBin.
  // For that we need to know how many events there are in the splitted bins already now (before deciding whether to split again or not)
  auto minbincabove = *std::min_element(bcs_above.begin(),bcs_above.end());
  auto minbincbelow = *std::min_element(bcs_below.begin(),bcs_below.end());
  if(minbincabove < m_MinNumberOfEventsPerBin || minbincbelow < m_MinNumberOfEventsPerBin){

    //try to find new splitbin that works for all histos
    auto find_new_splitbin = [this, &fill_bc_vectors, &dir, &bcs_below, &bcs_above, &binxlo, &binxhi, &binylo, &binyhi, &splitbin]
        (const unsigned int index, bool below) -> void {
      if(dir == Direction::X){
        if(below)
          while(m_Histos[index].Integral(binxlo,splitbin,binylo,binyhi) < m_MinNumberOfEventsPerBin) splitbin++;
        else
          while(m_Histos[index].Integral(splitbin+1,binxhi,binylo,binyhi) < m_MinNumberOfEventsPerBin) splitbin--;
      }
      else{ //Direction::Y
        if(below)
          while(m_Histos[index].Integral(binxlo,binxhi,binylo,splitbin) < m_MinNumberOfEventsPerBin) splitbin++;
        else
          while(m_Histos[index].Integral(binxlo,binxhi,splitbin+1,binyhi) < m_MinNumberOfEventsPerBin) splitbin--;
      }
      fill_bc_vectors();
    };

    //we also need to check the histos that have already been iterated over
    auto check_iterated_histos = [this, &bcs_below, &bcs_above, &binxlo, &binxhi, &binylo, &binyhi]
        (const unsigned int index) -> bool {
      for(auto j = index; j >= 0; j--){
        if(bcs_below[j] < m_MinNumberOfEventsPerBin || bcs_above[j] < m_MinNumberOfEventsPerBin){
          fill_new_bin(bcs_below[0],bcs_above[0],binxlo,binxhi,binylo,binyhi);
          return true;
        }
      }
      return false;
    };

    //iterate through histos, check which histos are below limit and try to find new splitbin. if this is not possible, fill a new bin
    for(decltype(m_NHistos) i = 0; i < m_NHistos; i++){
      if(bcs_below[i] < m_MinNumberOfEventsPerBin){
        find_new_splitbin(i,true);
        if(check_iterated_histos(i))
          return;
      }
      if(bcs_above[i] < m_MinNumberOfEventsPerBin){
        find_new_splitbin(i,false);
        if(check_iterated_histos(i))
          return;
      }
    }
  }

  if(m_ABmsg.GetMsgLvl() == MSG_LVL::DEBUG){
    TString msg = "Bincontents after split below median: ";
    for(auto bcsas : bcs_below)
      msg += TString::Format("%8.6g  ",bcsas);
    m_ABmsg.debugmsg(msg);
    msg = "Bincontents after split above median: ";
    for(auto bcsas : bcs_above)
      msg += TString::Format("%8.6g  ",bcsas);
    m_ABmsg.debugmsg(msg);
  }
  // set m_SmallestHisto for histograms below median
  if(m_NHistos > 1)
    dir == Direction::X ? get_least_entries_in_range(binxlo,splitbin,binylo,binyhi) : get_least_entries_in_range(binxlo,binxhi,binylo,splitbin);

  const auto min_sum_of_weights_below = *std::min_element(bcs_below.begin(),bcs_below.end());
  bool enough_input_bins_below = dir == Direction::X ? splitbin - binxlo > 2 : splitbin - binylo > 2;
  const float eps = m_MinNumberOfEventsPerBin*1e-3;
  // this can happen if there's a single input bin that has a weight larger than m_MinNumberOfEventsPerBin
  bool large_weight_below = abs(integral_subhist-min_sum_of_weights_below) < eps;

  // decide whether to split again or to fill a bin (below)
  if(min_sum_of_weights_below > 2*m_MinNumberOfEventsPerBin && enough_input_bins_below && !large_weight_below)
    dir == Direction::X ? split(binxlo,splitbin,binylo,binyhi,Direction::Y) : split(binxlo,binxhi,binylo,splitbin,Direction::X);
  else if(!final_attempt){
    m_ABmsg.debugmsg(TString::Format("split below xlo %d  xhi %d  ylo %d  yhi %d  splitbin %d  direction %c",binxlo,binxhi,binylo,binyhi,splitbin, dir == Direction::X ? 'X' : 'Y'));
    dir == Direction::X ? split(binxlo,splitbin,binylo,binyhi,Direction::Y,true) : split(binxlo,binxhi,binylo,splitbin,Direction::X,true);
  }
  else
    dir == Direction::X ? fill_new_bin(bcs_below[0],binxlo,splitbin,binylo,binyhi) : fill_new_bin(bcs_below[0],binxlo,binxhi,binylo,splitbin);

  // set m_SmallestHisto for histograms above median
  if(m_NHistos > 1)
    dir == Direction::X ? get_least_entries_in_range(splitbin+1,binxhi,binylo,binyhi) : get_least_entries_in_range(binxlo,binxhi,splitbin+1,binyhi);

  const auto min_sum_of_weights_above = *std::min_element(bcs_above.begin(),bcs_above.end());
  bool enough_input_bins_above = dir == Direction::X ? binxhi - (splitbin + 1) > 2 : binyhi - (splitbin + 1) > 2;
  bool large_weight_above = abs(integral_subhist-min_sum_of_weights_above) < eps;
  // decide whether to split again or to fill a bin (above)
  if(min_sum_of_weights_above > 2*m_MinNumberOfEventsPerBin && enough_input_bins_above && !large_weight_above)
    dir == Direction::X ? split(splitbin+1,binxhi,binylo,binyhi,Direction::Y) : split(binxlo,binxhi,splitbin+1,binyhi,Direction::X);
  else if(!final_attempt){
    m_ABmsg.debugmsg(TString::Format("split above xlo %d  xhi %d  ylo %d  yhi %d  splitbin %d  direction %c",binxlo,binxhi,binylo,binyhi,splitbin+1, dir == Direction::X ? 'X' : 'Y'));
    dir == Direction::X ? split(splitbin+1,binxhi,binylo,binyhi,Direction::Y,true) : split(binxlo,binxhi,splitbin+1,binyhi,Direction::X,true);
  }
  else
    dir == Direction::X ? fill_new_bin(bcs_above[0],splitbin+1,binxhi,binylo,binyhi) : fill_new_bin(bcs_above[0],binxlo,binxhi,splitbin+1,binyhi);

  if(large_weight_below || large_weight_above)
    m_ABmsg.warningmsg(TString::Format("Looks like the sum of weights in this bin x %d, %d y %d, %d of hist %s comes from a "\
                                       "single event and is larger than the minimum sum of weights. "\
                                       "Better check your config and inputs.",binxlo,binxhi,binylo,binyhi,m_SmallestHisto->GetName()));

  return;
}
