/**
  * @file make_many_TH2A_simple.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2018-09-28
  * @brief This script produces and saves several "reasonable" adaptively binned histograms from a single input histogram
  * @details Several adaptive binnings will be produced and saved to disk.
  * It starts from the coarsest binning possible (this binning has to be determined by the user in systematic studies and can be controlled by `GlobMinNBins`.
  * In such a study, a potential bias of the final result has to be monitored, since too coarse binnings might not capture all relevant information.).
  * Subsequent binnings are produced by decreasing the minimal number of events per bin by a factor two in each step. The procedure is stopped, once
  * the relative uncertainty on the average uncertainty rises above a factor `StatDecCoeff` compared to it's inital value from the coarsest binning.
  */


#include "TFile.h"
#include "TStopwatch.h"
#include <TH2A.h>
#include <IOjuggler.h>

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //parse options and initialise objects
  auto options = IOjuggler::parse_options(argc, argv, "d:i:ho:t:v:","nonoptions: <GlobMinNEventsPerBin> (default: 1), "
                                                                    "<GlobMinNBins> (default: 8), <StatDecCoeff> (default: 1.5)"
                                                                    "<new hist name> (default: old+_adaptive_#nbins#)",0);
  MessageService msgsvc("make_many_TH2A_simple",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  TH2A::SetVerbosity(options.get<int>("verbosity"));
  const auto wd           = options.get<std::string>("workdir");
  const auto histname     = options.get<std::string>("treename");
  auto infile             = IOjuggler::get_file(options.get<std::string>("infilename"),wd);
  TFile* outfile          = TFile::Open((wd+"/"+options.get<std::string>("outfilename")).data(),"RECREATE");

  //very very ugly... there has to be a better way
  auto GlobMinNEventsPerBin = 1.f;
  auto GlobMinNBins = 8.f;
  auto StatDecCoeff = 1.5;
  auto hname_stub = histname + "_adaptive";
  unsigned int aux_i = 0u;
  const auto oea = options.get_child_optional("extraargs");
  if(oea){
    for(const auto& ea : *oea){
      if(aux_i == 0) GlobMinNEventsPerBin = std::stod(ea.second.data());
      else if(aux_i == 1) GlobMinNBins = std::stod(ea.second.data());
      else if(aux_i == 2) StatDecCoeff = std::stod(ea.second.data());
      else hname_stub = ea.second.data();
      aux_i++;
    }
  }

  //let's get the coarsest possible histogram. for this, we divide the number of events (or sum of weights) by the desired number of bins.
  // e.g. if we have 1200 events and like to have 8 bins, there should ideally be 150 events in each bin. It is likely that the resulting binning
  // will fluctuate around these 150 events, e.g. having a bin with 149 and one with 151 entries.
  // This is prevented by setting the required minimal number of entries per bin lower. To have a general definition, we take the rounded up
  // number of events that we would get with 1 bin more. That is 134 in the example case (1200 events/9 bins).
  const TH2D* input_histo = IOjuggler::get_obj<TH2D>(infile,histname);
  const auto nevents_input = input_histo->GetSumOfWeights();
  msgsvc.infomsg("The sum of weights in the input histogram is "+ std::to_string(nevents_input));
  auto running_minneventsperbin = nevents_input/(GlobMinNBins+1.f);

  //now we can get all reasonable binnings by dividing the min number of events per bin by factors of 2
  // note that these are not all binnings that can be found. the factor of 2 comes from the case where the variables on the axes are uncorrelated.
  // in such a case, the number of bins will usually increase by factors of 2 (apart from bin migration effects) since only binary splits are performed.
  // TH2As are created and written to disk as long as the running minimal number of events per bin doesn't fall below it's global threshold or
  // the relative uncertainty of the weighted mean increases by more than the above configured factor
  // (remember that we are trading statistical precision with a finer binning against a better sensitivity to the shape which in turn reduces systematic uncertainties)
  outfile->cd();
  aux_i = 0;
  double global_rel_unc = 0.0, running_rel_unc = 0.0;
  while(running_minneventsperbin >= GlobMinNEventsPerBin && running_rel_unc <= StatDecCoeff*global_rel_unc) {
    TH2A h_ada;
    h_ada.MakeBinning(*input_histo,running_minneventsperbin);

    //average relative error per bin
    const auto current_nbins = h_ada.GetNBins();
    auto current_rel_unc = 0.0;
    for(unsigned int ibin = 1; ibin <= current_nbins; ibin++)
      current_rel_unc += h_ada.GetBinError(ibin)/h_ada.GetBinContent(ibin);
    current_rel_unc /= static_cast<float>(current_nbins);

    //some printout of the current histo
    msgsvc.infomsg(TString::Format("Each bin contains %.2f events on average",nevents_input/static_cast<float>(current_nbins)));
    msgsvc.infomsg(TString::Format("The relative uncertainty on the weighted mean is %.4f %%",100*current_rel_unc));
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) h_ada.Print();
    msgsvc.infomsg(std::string(128,'*'));

    //set weighted mean uncertainties for breaking criteria of the loop
    if(aux_i == 0)
      global_rel_unc = current_rel_unc;
    running_rel_unc = current_rel_unc;
    h_ada.SetName((hname_stub + "_" + std::to_string(current_nbins)).data());
    h_ada.Write();

    //divide the minimum number of events per bin by 2 to get a finer binning in the next execution of the loop
    running_minneventsperbin /= 2.;

    aux_i++;
  }
  outfile->Close();//avoid double free corruption

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}
