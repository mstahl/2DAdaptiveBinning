/**
  * @file ab00.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2016-08-23
  * @brief This tutorial shows the basic usage of TH2A
  */

#include "TROOT.h"
#include "TCanvas.h"
#include "TF2.h"
#include "TH2D.h"

#include <utility>

#include "lhcbStyle.h"
#include "TH2A.h"

int main(int argc, char** argv){

  lhcbStyle();
  //Set the verbosity to DEBUG. Definition in MessageService.h enum class MSG_LVL{ERROR = 0, WARNING, INFO, DEBUG};
  TH2A::SetVerbosity(3);

  //define a 2D function
  TF2 *f2 = new TF2("f2","xygaus + xygaus(5) + xylandau(10)",-4,4,-4,4);
  Double_t params[] = {130,-1.4,1.8,1.5,1, 150,2,0.5,-2,0.5, 3600,-2,0.7,-3,0.3};
  f2->SetParameters(params);
  //sample a histogram using random numbers
  TH2D h1("h1","xygaus + xygaus(5) + xylandau(10)",1000,-4,4,1000,-4,4);
  h1.FillRandom("f2",200000);
  //make another histogram with different parameters
  params[0] = 600;//just make the first gaussian stronger
  f2->SetParameters(params);
  TH2D h2("h2","xygaus + xygaus(5) + xylandau(10)",1000,-4,4,1000,-4,4);
  h2.FillRandom("f2",200000);

  //make an adaptive binning which has at least 500 events in every bin of the input histograms h1 and h2  
  TH2A h1_adaptive;
  h1_adaptive.MakeBinning(std::vector<TH2D>{h1,h2},500.0);
  h1_adaptive.Print();
  TH2A h2_adaptive;
  h2_adaptive = h1_adaptive;//copy assignment operator copies only binning
  //auto h2_adaptive = h1_adaptive; or TH2A h2_adaptive(h1_adaptive); does the same
  h2_adaptive.FillFromHist(h2);//therefore the histogram still needs to be filled
  //write binning to disk
  h1_adaptive.WriteBinning("binnings/tutorial_binning.adb");

  TCanvas c1("c1","Canvas for adaptive binning",10,10,2048,1280) ;
  gPad->SetTopMargin(0.03);
  gPad->SetBottomMargin(0.15);
  gPad->SetRightMargin(0.13);
  gPad->SetLeftMargin(0.05);

  h1_adaptive.SetXaxisTitle("my awesome title");
  h1_adaptive.Draw();//default is "" which corresponds to "colz" or "COLZ". No other options are implemented so far
  if(!gSystem->OpenDirectory("plots"))gSystem->mkdir("plots");
  c1.SaveAs("plots/ab00_h1.pdf");
  c1.Clear();
  h2_adaptive.SetXaxisTitle("my super awesome title");
  h2_adaptive.SetYaxisTitle("plop");
  gPad->SetLeftMargin(0.15);
  h2_adaptive.Draw();
  c1.Update();
  c1.SaveAs("plots/ab00_h2.pdf");

  //let's have a look how the binning of h1 without h2's presence looks like
  TH2A h1_adaptive_single;
  h1_adaptive_single.SetVerbosity(2);//only INFO, WARNING and ERROR messages
  //the axis tiltes are adopted from the first histogram in TH2A::MakeBinning()
  h1.GetXaxis()->SetTitle("apples");
  h1.GetYaxis()->SetTitle("bananas");
  h1_adaptive_single.MakeBinning(h1,500.0);//call overloaded function
  c1.Clear();
  h1_adaptive_single.Draw();
  c1.SaveAs("plots/ab00_h1_single.pdf");

  delete f2;
  return 0;
}

