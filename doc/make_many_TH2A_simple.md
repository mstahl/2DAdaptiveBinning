# Documentation for make_TH2A_simple
This script produces and saves several "reasonable" adaptively binned histograms from a single input histogram (castable to TH2D).
It starts from the coarsest binning possible (this binning has to be determined by the user in systematic studies and can be controlled by the global minumum number bins (`GlobMinNBins`).
In such a study, a potential bias of the final result has to be monitored, since too coarse binnings might not capture all relevant information.).
Subsequent binnings are produced by decreasing the minimal number of events per bin by a factor two in each step. The procedure is stopped, once
the uncertainty on the average uncertainty rises above a factor `StatDecCoeff` compared to it's inital value from the coarsest binning.

The adaptive binning algorithm is documented [here](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning) with code docu [here](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/annotated.html).<br>

## Arguments for the executable
- `-d` : work directory
- `-i` : input file name
- `-o` : ouput file name
- `-t` : **histogram** name (not tree!)
- `-v` : verbosity (1-3, optional)
- nonoptions: (Note the order of the options. You need to give the defaults if you just want to change e.g. the name stub)
    - global minumum number of events per bin (`GlobMinNEventsPerBin`, default 1)
    - global minumum number bins (`GlobMinNBins`, default 8)
    - factor above which the average uncertainty is not allowed to rise (`StatDecCoeff`, default 1.5)
    - name stub of output histogram (`new hist name`, default old name + "_adaptive").
