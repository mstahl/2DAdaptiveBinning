# Getting started
To setup and configre gitlab properly, visit the
[gitlab help sites](https://gitlab.cern.ch/help/gitlab-basics/README.md "gitlab help sites")

Then clone this repository
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/mstahl/2DAdaptiveBinning.git
```
or make a fork and clone
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/<your_username>/2DAdaptiveBinning.git
```

In case you forked, it is recommended to add this repo as upstream
```
cd 2DAdaptiveBinning;
git remote add ssh://git@gitlab.cern.ch:7999/mstahl/2DAdaptiveBinning.git
```

For development, it is best to make new branches
```
git checkout -b <devel_branch>
```

and - ideally - delete them when merging them into the upstream repository.
This avoids a complicated branch structure and prevents merge conflicts.

# Usage
Setup your environment and compiler. For example:
```
source setup.sh lb-conda default
```

In the simplest case, you might only be interested to get an adaptively binned histogram from a TH2D input histogram.
For this, check out the [Generate an adaptive binning from 2D histograms](#generate-an-adaptive-binning-from-2d-histograms) section below.

It's worth to take a look at the tutorials in
`src/abXX.cpp` to learn how to use the adaptive binning in your code.
To run the tutorials, do something like
```
cmake -S . -B build; cmake --build build -j; build/bin/ab00
```

# Features
Compared to the adaptive binning implemented in ROOT,
this algorithm allows to find a common adaptive binning for several input datasets.
This is especially interesting for studying efficiencies, where you need to
divide 2 histograms with the same binning.


# Available tutorials
The tutorials are located in the `src` directory
- ab00.cpp shows the basic usage of TH2A
- ab01.cpp shows how to construct a TH2A object from an existing binning (can be created by TH2A::WriteBinning())
- ab02.cpp shows how to fill a TH2A with efficiencies from TEfficiency and combine 2 efficiencies
- ab03.cpp shows how to fill a TH2A from a Dalitz plot and draw it with it's boundary

# Generate an adaptive binning from 2D histograms
A generic script to generate an adaptive binning is in [src/make_TH2A.cpp](src/make_TH2A.cpp). It works with boost property-trees and is documented [here](doc/make_TH2A.md "doc/make_TH2A.md").<br>
The simplified version of that script ([src/make_TH2A_simple.cpp](src/make_TH2A_simple.cpp)) can only take one input histogram, and therefore does not need a config file.<br>
Systematic studies of the binning for a single input histogram can be carried out with [src/make_many_TH2A_simple.cpp](src/make_many_TH2A_simple.cpp).
It automatically creates and saves binning schemes in a range that is configurable by intuitive meta-parameters ([click here for details](doc/make_many_TH2A_simple.md "doc/make_many_TH2A_simple.md"))


# TH2A and TFile
TH2A doesn't follow the usual ROOT object ownership convention. The TH2A object is not destructed when closing the file. <br>
This means that the object needs to be deleted by hand before closing the file to avoid double free/corruption errors.
It is however possible to close the file before calling a TH2A-method. In this case, the error will still occur if the object hasn't been deleted and goes out of scope.<br>
The following works:
```
root [0] .L build/lib/libTH2A.so
root [1] TFile f("myfile.root")
root [2] TH2A& adhist = *static_cast<TH2A*>(f.Get("storedTH2A"))
root [3] f.Close()
root [4] adhist.Draw()
root [5] delete &adhist
root [6] .q
```
Of course it also works with pointers directly. Note that `delete &adhist` has to be done before `f.Close()` when calling `adhist.Draw()` before `f.Close()`!

# Copying TH2A objects
Since TH2A objects are quite heavy, the copy assignment operator and constructor only copy the underlying binning. If a copy is really needed, one should call `TH2A::Clone("new_name")` as inherited from `TNamed`

# Integrate TH2A into your CMake project
There is a CMake find-module which sets variables and also generates the ROOT dictionary and the dynamical library. For an example usage have a look at the local [CMakeLists.txt](CMakeLists.txt) or at the [LcD0K project](https://gitlab.cern.ch/lhcb-bandq-exotics/Lb2LcD0K/blob/master/CMakeLists.txt) where TH2A is integrated by setting the `TH2A_DIR` environment variable.

# Doxygen
A doxygen documentation is available [here](http://physi.uni-heidelberg.de/~mstahl/2DAdaptiveBinning/html/annotated.html "here") <br>
To build it, you can use cmake
```
cd build; make doc; cd ..;
```
or if there is some issue with the environment, it's easiest to run doxygen by hand, like e.g.
```
/cvmfs/lhcb.cern.ch/lib/lcg/releases/doxygen/1.8.11-ae1d3/x86_64-slc6-gcc49-opt/bin/doxygen include/Doxyfile_noCMake.in
```
