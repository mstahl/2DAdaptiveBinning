/**
  * @file ab03.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2016-08-22
  * @brief This tutorial shows how to use DrawDPBoundary
  */

#include "TROOT.h"
#include "TCanvas.h"
#include "TH2F.h"
#include "TFile.h"
#include "TStyle.h"
#include "TPaletteAxis.h"
#include "TStopwatch.h"

#include "lhcbStyle.h"
#include "TH2A.h"

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  lhcbStyle();

  TFile myfile("files/Lc_Dalitz.root","READ");
  //note that the Dalitz plot in the file has 2k*2k bins! this is needed for good performance of the adaptive binning
  TH2F* Lc_dp = static_cast<TH2F*>(myfile.Get("dalitz"));

  //make an adaptive binning which has at least 100 events in every bin of the input histogram
  TH2A dp_adaptive;
  dp_adaptive.MakeBinning(*Lc_dp,100);

  TCanvas c1("c1","Canvas for adaptive binning",10,10,2048,1280) ;
  gPad->SetTopMargin(0.06);
  gPad->SetBottomMargin(0.15);
  gPad->SetRightMargin(0.13);
  gPad->SetLeftMargin(0.15);

  dp_adaptive.SetXaxisTitle("M_{inv}^{2} (pK^{-}) (GeV^{2})");
  dp_adaptive.SetYaxisTitle("M_{inv}^{2} (K^{-}#pi^{+}) (GeV^{2})");
  gStyle->SetPalette(56);
  dp_adaptive.Draw();
  dp_adaptive.DrawDPBoundary("Lambda_c+","proton","K-","pi-",1e+4);

  //get the palette from the static dummy histo which is created in TH2A::Draw()
  TPaletteAxis* palette = static_cast<TPaletteAxis*>(static_cast<TH2D*>(gROOT->FindObjectAnyFile("dummy"))->GetListOfFunctions()->FindObject("palette"));
  palette->SetY1NDC(gPad->GetBottomMargin()+gPad->GetTopMargin());

  if(!gSystem->OpenDirectory("plots"))gSystem->mkdir("plots");
  c1.SaveAs("plots/ab03_Lc_Dalitz.pdf");

  clock.Stop();
  clock.Print();

  return 0;
}
